#pragma once
#include"MyReadTxt.h"
#include"MyUtility.h"
#include"MyBook.h"
#include"MyPageBase.h"
#include"MyClient.h"

#include <conio.h>
#include <graphics.h>
#include <time.h>
#include <stdlib.h>

#include <functional>
#include <utility>
#include <vector>
#include <deque>
#include <exception>
#include <stdexcept>
#include <map> 




/**************************  PageAdministrator  页面管理类    ******************************/
class PageAdministrator : public PageAdministratorBase{
	friend class SingletonPattern<PageAdministrator>;
//私有方法
private:
	PageAdministrator();									//用户
	PageAdministrator(int k);								//管理员
	~PageAdministrator();
	void Loading();											//加载 显示加载页

//公有数据
public:
	static std::shared_ptr<Page> mainPage;					//主页面指针
	static std::shared_ptr<Page> mainAdministratorPage;		//管理员主页面指针
	static BookstoreAdministrator& bookstoreAdministrator;	//图书管理类
	static ConsumerAdministrator& consumerAdministrator;	//用户管理类
	static int bookCount;									//当前书计数

//公有方法
public:
	void Import();											//输入函数

protected:
	virtual std::shared_ptr<Page> StackRollback() override;		//栈回退
};


/**************************  PageAdministrator  页面管理类  内联函数  **************************/
//页面栈回退函数
inline std::shared_ptr<Page> PageAdministrator::StackRollback() {
	FlagStackRollback = false;
	std::shared_ptr<Page> p;
	while (!stackPagesP.empty()) {
		p = stackPagesP.back();
		stackPagesP.pop_back();
		if (p != nullptr)
			break;
	}
	if(p != nullptr)
		return p;
	else
	return PageAdministrator::mainAdministratorPage != nullptr
		? PageAdministrator::mainAdministratorPage
		: PageAdministrator::mainPage;
}

//输入函数
inline void PageAdministrator::Import() {
	Loading();
	PageAdministratorBase::Import();
}


/*****************************     MainPage1_2      1.2主页类      ***************************/
/*
* 基类： 页面类
* 新增： 图书数组
* 说明：每次加载20个图书
* 如果用户点击“加载更多”，那么会先导入到图书数组里面（静态），然后复制本身类，再更新数据显示
* 如果直接修改本页会导致 线程冲突，所有选择以上方法
*/
class MainPage1_2 : public DynamicPage {

public:
	static std::vector<Book> books;							//图书数组
	int bookCount = 0;										//图书计数
	int bookCountMax = 0;
	static Book b1, b2;										//查找使用的book
	static bool FlagIniBook;								//初始化完成标志
	bool FlagAddBook = true;								//增加标志
	BookstoreAdministrator& bookstoreAdministrator
		= PageAdministrator::bookstoreAdministrator;		//引用图书管理类

//公有方法
public:
	MainPage1_2();											//用户
	MainPage1_2(const MainPage1_2&) = default;				//
	static inline void RestoreFindBook();					//恢复查找使用的书

private:
	inline void computeDynamicCountMax(int off);			//计算最大下滑量
	inline void FlushBooks();								//刷新引用的图书数组
	void AddBooks();										//增加书
	inline void AddSearchFun();								//搜索功能
	
};


/*************************     MainPage1_2      1.2主页类    内联函数    *********************/
//刷新查找
void MainPage1_2::RestoreFindBook() {
	b1.name = L"";
	b1.price = 0;
	b2.price = ImportDataNumber::w_max;
	b1.repertory = 0;
	b2.repertory = ImportDataNumber::w_max;
	b1.sale = 0;
	b2.sale = ImportDataNumber::w_max;
	FlagIniBook = true;
}

//计算并设置窗口最大滑动
void MainPage1_2::computeDynamicCountMax(int off) {
	int k = (bookCountMax + 3) / 4;
	int count = k * -off + 480 - 120;
	dynamicCountMax = count < 0 ? count : 0;
	
}

//刷新图书数组
void MainPage1_2::FlushBooks() {
	books.clear();
	bookCount = 0;
	bookstoreAdministrator.BooksFind(b1, b2, books, 0, 20);
	bookCountMax = static_cast<int>(books.size());
	computeDynamicCountMax(220);
	RestoreFindBook();
	FlagAddBook = true;

}




/***********************     AdministratorMainPage      管理员主页类      ********************/
/*
* 基类： 页面类
* 新增： 图书管理类
*/
class AdministratorMainPage : public Page {
	//公有方法
public:
	AdministratorMainPage();								//管理员
};


/******************************     BookPage     书详细信息页面类      ************************/
/*
* 基类： 页面类
* 新增： 书的数据
*/
class BookPage : public Page {
//公有方法
public:
	explicit BookPage(Book& book);							//BookPage构造函数

//公有成员
public:
	Book& book;												//BookPage独有成员
	BookstoreAdministrator& bookstoreAdministrator
		= PageAdministrator::bookstoreAdministrator;			//引用图书管理类
};


/*****************************    ShoppingPage  购物车页面      ******************************/
/*
* 基类： 动态页面类
* 新增： 购物车
*/
class ShoppingPage : public DynamicPage {
//公有方法
public:
	ShoppingPage();

//私有方法
private:
	inline void computeDynamicCountMax();						//计算最大下滑量
	std::vector<std::pair<int, int>> data;						//商品价格 商品数量
	std::vector<bool> flag;										//选中标志

//公有成员
public:
	BookstoreAdministrator& bookstoreAdministrator
		= PageAdministrator::bookstoreAdministrator;			//引用图书管理类
};


/************************************  Thanks   致谢   *********************************/
class Thanks {
//友元
	friend class Read<Thanks>;

//公有成员
public:
	std::wstring name;					//名字
	std::wstring postion;				//图片位置

//公有方法
public:
	void read(std::wifstream& file) { Read<Thanks>::Fun(*this, file); }

//私有方法
private:
	void get_name(std::wstring str) { name = str; };
	void get_postion(std::wstring str) { postion = str; };
};


/************************************  ThanksPage   致谢页   ************************************/
/*
* 基类： 动态页面类
* 新增： 致谢者数组
*/
class ThanksPage : public DynamicPage {
//公有方法
public:
	ThanksPage();

//私有方法
private:
	inline void computeDynamicCountMax();						//计算最大下滑量

//公有成员
public:
	static std::vector<Thanks> thanksArr;						//
	constexpr static char postion[] = "thanks\\thanks.txt";		//
};

///******************************     LoadingPage     加载页      *********************************/
///*
//* 基类： 页面类
//* 新增： 书的数据
//*/
//class LoadingPage : public Page {
//	//公有方法
//public:
//	LoadingPage() = default;
//};









/*******************************       Logo      日志     ************************************/
struct LogoAdministrator;				//logo管理类 提前声明

struct Logo {
	friend struct LogoAdministrator;
	friend class Read<Logo>;

//公有方法
public:
	void Show() {
		for (int i = 0; i < diary.size(); i++) {
			show::showWordsL(diary[i], x1, x2, y1 + i * 30, y2 + i* 30, 24);}
	}																	//显示日志

//私有方法
private:
	Logo() noexcept {};													//默认构造									
	void set_diary(std::wstring str) {
		diary.push_back(str);
	};																	//set函数
	void read(std::wifstream& file) { Read<Logo>::Fun(*this, file); }	//read

//私有数据
private:
	constexpr static char postion[] = "pages\\logos.txt";		//文档位置
	constexpr static int x1 = 160, x2 = 700, y1 = 50, y2 = 80;	//显示坐标
	std::vector<std::wstring> diary;								//日志
};


/*********************     LogoAdministrator      logo管理类      ****************************/
struct LogoAdministrator {

//友元
	friend class SingletonPattern<LogoAdministrator>;
	friend class Read<LogoAdministrator>;

//公有成员
public:
	static std::vector<Logo> logos;							//logo 日志数组
	static int count;										//计数

//公有方法
	LogoAdministrator();									//默认构造


//私有方法
private:
	void set_count(std::wstring str){ count = std::stoi(str);};						//set
	void read(std::wifstream& file) { Read<LogoAdministrator>::Fun(*this, file); }	//read

//私有成员
private:
	constexpr static char postion[] = "pages\\logos.txt";	//文档位置
};


/*****************************    LogoPage   logo页      ******************************/
class LogoPage : public Page {
public:
	LogoPage();												//主页面构造函数
	virtual void ShowPage() override;						//画面函数
public:
	LogoAdministrator& logoAdministrator 
		= *SingletonPattern<LogoAdministrator>::Get();		//单例 logo管理类
	static int count;										//logo类的计数
	static int countMax;

};

/*页面绘制函数*/
inline void LogoPage::ShowPage() {
	Page::ShowPage();										//调用基类画面
	if (count < logoAdministrator.count) {
		logoAdministrator.logos[count].Show(); 
	}														//显示日志
};


/************************************  PaymentPage   支付页   ************************************/
/*
* 基类： 动态页面类
*/
class PaymentPage : public DynamicPage {
//公有方法
public:
	PaymentPage();

//私有方法
private:
	inline void computeDynamicCountMax(int off,int k);			//计算最大下滑量

//公有成员
public:
	BookstoreAdministrator& bookstoreAdministrator
		= PageAdministrator::bookstoreAdministrator;			//引用图书管理类
};


/*********************   AdministratorBookstore   管理员查看商户   **********************/
/*
* 基类： 页面类
*/
class AdministratorBookstore : public Page {
//公有方法
public:
	AdministratorBookstore();

//公有成员
public:
	static int maxPage;											//最大页
	static int atPage;											//当前页
	static int atCount;											//当前商户
	static int maxCount;										//最大商户数量
	constexpr static int aPage = 10;							//一页10个
	BookstoreAdministrator& bookstoreAdministrator
		= PageAdministrator::bookstoreAdministrator;			//引用图书管理类
	static std::vector<Bookstore> bookstores;					//复制一份商户列表
	static int FlagSort;										//排序标志



};


/*********************   AdministratorBook   管理员查看图书   **********************/
/*
* 基类： 页面类
*/
class AdministratorBook : public Page {
//公有方法
public:
	AdministratorBook();

//公有成员
public:
	static int maxPage;											//最大页
	static int atPage;											//当前页
	static int atCount;											//当前商户
	static int maxCount;										//最大商户数量
	constexpr static int aPage = 10;							//一页10个
	BookstoreAdministrator& bookstoreAdministrator
		= PageAdministrator::bookstoreAdministrator;			//引用图书管理类
	static std::vector<Book> books;								//复制一份商户列表
	static int FlagSort;										//排序标志
};



/*****************   AdministratorWriteBookstore  管理员书店数据结构   *********************/
class AdministratorWriteBookstore : public Bookstore {
public:
	AdministratorWriteBookstore() : Bookstore() {};
	using Bookstore::bookPosition;
	using Bookstore::read;
	using Bookstore::write;
};


/*******************  AdministratorAddBookstore   管理员增加商户   ********************/
/*
* 基类： 页面类
*/
class AdministratorAddBookstore : public Page {
	//公有方法
public:
	AdministratorAddBookstore();

	//公有成员
public:
	BookstoreAdministrator& bookstoreAdministrator
		= PageAdministrator::bookstoreAdministrator;			//引用图书管理类
	static AdministratorWriteBookstore data;
	static std::wstring position;
	static std::wstring portrait;

};


/*********************     AdministratorAddBook    管理员增加书     ********************/
/*
* 基类： 页面类
*/
class AdministratorAddBook : public Page {
	//公有方法
public:
	AdministratorAddBook();

//公有成员
public:
	BookstoreAdministrator& bookstoreAdministrator
		= PageAdministrator::bookstoreAdministrator;			//引用图书管理类
	static Book bookData;
	static std::wstring bookPostion;							//图书位置
	static std::wstring postion;								//商家位置
	static std::wstring name;									//商家
	static std::wstring portrait;								//头像
	static int indexId;											//引索
	static bool FlagAdd;										//商家加载标志
	static bool FlagPhoto;										//图片加载标志

//公有方法
	void flush();
	void write();
	//void findBookstore();
	//void ReadBookstore();
	void findBookPhoto();
};

inline void AdministratorAddBook::flush() {
	bookPostion = L"";
	bookData.name = L"";
	bookData.price = 0;
	bookData.repertory = 0;
	FlagPhoto = false;
}

inline void AdministratorAddBook::write() {
	if (!FlagAdd || name == L"" || bookData.name == L"" || !FlagPhoto
		|| bookData.price <= 0 || bookData.repertory <= 0)
		return ;
	bookData.bookstoreId = indexId;
	std::wofstream file(postion, std::ios::app);
	bookData.write(file);
}

inline void AdministratorAddBook::findBookPhoto() {
	bookData.photo_200x150 = L"books\\BooksPhoto\\3_" + bookPostion;
	bookData.photo_100x75 = L"books\\BooksPhoto\\1_" + bookPostion;
	bookData.photo_140x105 = L"books\\BooksPhoto\\2_" + bookPostion;
	FlagPhoto = true;
}


/***************************     LoginPage    登入页     ************************/
/*
* 基类： 页面类
*/
class LoginPage : public Page {
public:
	LoginPage();

private:
	std::wstring name;							//
	std::wstring key;							//
	int flag = 0;								//错误标志
};



/***************************     RegisterPage    注册页     ************************/
/*
* 基类： 页面类
*/
class RegisterPage : public Page {
public:
	RegisterPage();

private:
	std::wstring name;							//
	std::wstring key;							//
	std::wstring key2;							//
	int flag = 0;								//错误标志
	int head = 0;								//头像
};


/***************************     LoadingPage    加载页     ************************/
/*
* 基类： 页面类
*/
class LoadingPage : public Page {
	struct LoadingPageData {
		std::vector<std::wstring> wstrs;
		void show();
	};
public:
	LoadingPage();
	LoadingPageData data;

public:
	virtual void ShowPage() override;
};


/********************     LoadingPage    加载页   内联函数     ************************/
//画面函数
inline void  LoadingPage::ShowPage(){
	Page::ShowPage();										//调用基类画面
	data.show();
};

//内部数据的画面函数
inline void LoadingPage::LoadingPageData::show() {
	for (int i = 0; i < wstrs.size(); i++) {
		show::showWordsL(wstrs[i], 200, 800, 50 + i * 40, 80 + i * 40, 24);
	}
}


/***************************     WalletPage    钱包页     ************************/
/*
* 基类： 页面类
*/
class WalletPage : public Page {

public:
	WalletPage();

public:
	Consumer& consumer = *SingletonPattern<Consumer>::Get();
	void set_vaule(ClickHandoverBool* p, int k);
	void set_way(ClickHandoverBool* p, int k);
	void pay();
private:
	ClickHandoverBool* p1 = nullptr;
	ClickHandoverBool* p2 = nullptr;
	int value = 0;
	int way = -1;

};

inline void WalletPage::set_vaule(ClickHandoverBool* p, int k) {
	if (p1 != nullptr) {p1->flag = false;}
	p1 = p;
	if (p1 != nullptr) { p1->flag = true; }
	value = k;
}

inline void WalletPage::set_way(ClickHandoverBool* p, int k) {
	if (p2 != nullptr) { p2->flag = false; }
	p2 = p;
	if (p2 != nullptr) { p2->flag = true; }
	way = k;
}

inline void WalletPage::pay() {
	consumer.pay(value);
}


/*********************              页面通用函数           ********************/

//页面通用函数
namespace PageFun {
	//回退 功能
	inline void AddRollbackFun(Page& page) {
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 720;											//设置四坐标
		ckP->x2 = 780;
		ckP->y1 = 0;
		ckP->y2 = 60;
		ckP->high = 24;
		ckP->position = L"pages\\ico0.png";					//图标
		std::function<std::shared_ptr<Page>()> p = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;			//不可回退
			PageAdministrator::FlagStackRollback = true;		//设置栈回退
			return nullptr;
		};
		ckP->fun = p;
		page.clicks.push_back(ckP);
	}

	//回退 功能（滚轮移动版本）
	inline void AddRollbackFun_Dynamic(DynamicPage& page) {
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 720;											//设置四坐标
		ckP->x2 = 780;
		ckP->y1 = 0;
		ckP->y2 = 60;
		ckP->high = 24;
		ckP->position = L"pages\\ico0.png";					//图标
		std::function<std::shared_ptr<Page>()> p = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;			//不可回退
			PageAdministrator::FlagStackRollback = true;		//设置栈回退
			return nullptr;
		};
		ckP->fun = p;
		page.dynamicClicks.push_back(ckP);
	}


	//退出
	inline void AddExitkFun(Page& page) {
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 740;
		ckP->x2 = 800;
		ckP->y1 = 445;
		ckP->y2 = 475;
		ckP->high = 24;
		ckP->words = L"退出";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagEnd = true;
			return nullptr; };
		page.clicks.push_back(ckP);
	}

	//1.1版本
	inline void AddVersionFun(Page& page) {
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 700;
		nckP->x2 = 800;
		nckP->y1 = 0;
		nckP->y2 = 30;
		nckP->words = L"版本1.1";
		page.noClicks.push_back(nckP);
	}

	//1.2版本
	inline void AddVersionFun1_2(Page& page) {
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 720;
		nckP->x2 = 800;
		nckP->y1 = 0;
		nckP->y2 = 25;
		nckP->high = 18;
		nckP->words = L"版本1.2";
		page.noClicks.push_back(nckP);
	}

	//1.3版本
	inline void AddVersionFun1_3(Page& page) {
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 720;
		nckP->x2 = 800;
		nckP->y1 = 0;
		nckP->y2 = 25;
		nckP->high = 18;
		nckP->words = L"版本1.3";
		page.noClicks.push_back(nckP);
	}

	//1.4版本
	inline void AddVersionFun1_4(Page& page) {
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 720;
		nckP->x2 = 800;
		nckP->y1 = 0;
		nckP->y2 = 25;
		nckP->high = 18;
		nckP->words = L"版本1.4";
		page.noClicks.push_back(nckP);
	}

	//进入管理员模式
	inline void AccessAdministratorPage(Page& page) {
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 705;
		ckP->x2 = 795;
		ckP->y1 = 350;
		ckP->y2 = 440;
		ckP->high = 24;
		std::function<std::shared_ptr<Page>()> p = []()->std::shared_ptr<Page> {
			return PageAdministrator::mainAdministratorPage; };
		ckP->fun = p;
		ckP->position = L"pages\\ico12.jpeg";
		page.clicks.push_back(ckP);
	}

	//进入设置
	inline void AccessSetPage(Page& page) {
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 5;
		ckP->x2 = 95;
		ckP->y1 = 380;
		ckP->y2 = 470;
		ckP->high = 24;
		std::function<std::shared_ptr<Page>()> p = []()->std::shared_ptr<Page> {
			return PageAdministrator::mainAdministratorPage; };
		ckP->fun = p;
		ckP->position = L"pages\\ico17.jpeg";
		page.clicks.push_back(ckP);
	}


	//钱包
	inline void AccessWalletPage(Page& page) {
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 5;
		ckP->x2 = 95;
		ckP->y1 = 170;
		ckP->y2 = 260;
		ckP->high = 24;
		std::function<std::shared_ptr<Page>()> p = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new WalletPage); };
		ckP->fun = p;
		ckP->position = L"pages\\ico13.jpg";
		page.clicks.push_back(ckP);
	}
	//购买记录
	inline void AccessShoppingRecordPage(Page& page) {
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 5;
		ckP->x2 = 95;
		ckP->y1 = 270;
		ckP->y2 = 375;
		ckP->high = 24;
		std::function<std::shared_ptr<Page>()> p = []()->std::shared_ptr<Page> {
			return PageAdministrator::mainAdministratorPage; };
		ckP->fun = p;
		ckP->position = L"pages\\ico14.jpeg";
		page.clicks.push_back(ckP);
	}
	//logo点击
	inline void AccessLogoPage(Page& page){
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 0;
		ckP->x2 = 100;
		ckP->y1 = 0;
		ckP->y2 = 75;
		std::function<std::shared_ptr<Page>()> p = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new LogoPage); };
		ckP->fun = p;
		ckP->position = L"pages\\ico16.png";
		page.clicks.push_back(ckP);
	}
	//购物车
	inline void AccessShoppingPage(Page& page) {
		std::shared_ptr<Click> ckP(new Click);
		Click ck;
		ckP->x1 = 5;
		ckP->x2 = 100;
		ckP->y1 = 85;
		ckP->y2 = 160;
		ckP->position = L"pages\\ico15.jpg";
		std::function<std::shared_ptr<Page>()> p = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new ShoppingPage);
		};
		ckP->fun = p;
		page.clicks.push_back(ckP);
	}
};

