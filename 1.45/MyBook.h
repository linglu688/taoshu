#pragma once
#ifndef MYBOOK_INCLUDED
#define MYBOOK_INCLUDED

#include"MyReadTxt.h"
#include"MyUtility.h"
#include"MySingletonPattern.h"

#include<string>
#include<unordered_map>
#include<vector>
#include<algorithm>
#include<set>
#include<map>


/*
接口说明
	read write 说明参见MyReadTxt

1.Book 书 类 
	方法：
	void read(std::ifstream& file) 
	void write(std::ofstream& file)
	成员：
	std::string name;								//名字
	std::string photo_200x150;						//图片位置 高150 x 宽200
	std::string photo_100x75;						//图片位置 高75 x 宽100
	std::string photo_140x105;						//图片位置 高105 x 宽140
	int price = -1;									//价格
	int bookstoreId = 0;							//商家id
	mutable int repertory = 0;						//库存
	mutable int sale = 0;							//销量
	注： Book 提供比较 < 可以存 map

2. Bookstore  书店类
	方法：
	void read(std::ifstream& file)					*保护
	void write(std::ofstream& file)					*保护
	成员：
	std::string name;								//书店名
	std::set<Book> books;							//图书集合
	std::string portrait;							//头像
	long long income = 0;							//收入
	int id = 0;										//书店id
	int booksCount = 0;								//图书数量
	int sale = 0;									//销量
	std::string bookPosition;						//图书文件位置 *保护

3.BookstoreAdministrator  书店管理类
	//购物车相关 方法
	int& ShoppingBookCount(Book& book)				//查询购物车中book数量 如果没有会创建一个数量为0的对象
	void ShoppingEraser(Book& book)					//删除购物车里面book
	int ShoppingSize()								//购物车商品数量
	iterator ShoppingEnd()							//购物车最后一个元素后面的迭代器（不存在）
	iterator ShoppingBegin()						//购物车第一个元素迭代器
	bool ShoppingFind(Book book)					//购物车内是否存在book


	inline const Book& operator()(Book book);					//查找功能 图书是否存在
	inline const Book& find(Book book);							//查找功能 图书是否存在
	int BooksFind(Book& book1,Book& book2,std::vector<Book>& arr,int h, int b);
																//查找图书并将结果加入传入的数组
	解释：1.前两个Book为搜索条件，搜索规则：
	（1.价格 销量 库存 必须满足 大于等book1（前） 小于等于book2（后）
	（2.名字必须包含book1.name （前），book2.name不影响搜索结果
	2.arr为要写入结果的数组;
	3.h为起始位置（下标1开始）
	4.b为最多返回的数量
	5.返回值为正在返回的数量
	例子：如果需要查找 名字包含“C++” 价格100 - 200 销量大于30的书 的第10个到19个(10个元素)
	需要设置	book1.name = "C++" 
				book1.price = 100 book2.price = 200
				book1.sale = 30 book2.sale = 9999(很大的数)
				b = 10
				h = 19 - 10 + 1 = 10;
	如果名字包含“C++” 价格100 - 200 销量大于30的书 只有16本
				那么后返回7 10 - 16 一个7个元素

	成员：
	std::unordered_map<int, Bookstore> bookstores;				//书店表 商家id - 书店
	std::vector<Book> books;									//图书表
	constexpr static int cs_shoppingMax = 20;					//购物车最大量

*/

/**************************************   BookstoreAdministrator   ************************************/
class BookstoreAdministrator;			//图书管理类 提前声明 以便获取友元
class Bookstore;						//书店类	 提前声明


/***************************************   book   书     ********************************/
class Book{

//友元
	friend class BookstoreAdministrator;					//给书店管理类 友元
	friend class Bookstore;									//给书店类 友元
	friend class Read<Book>;								//读txt
	friend class Write<Book>;								//写txt

//公有方法
public:		
	bool operator < (const Book& book2) const {									// < map
		return  bookstoreId < book2.bookstoreId
			|| bookstoreId == book2.bookstoreId && name < book2.name;
	}
	void read(std::wifstream& file) { Read<Book>::Fun(*this, file); }			//read
	void write(std::wofstream& file) const { Write<Book>::Fun(*this, file); }	//write

	Book() noexcept {}															//默认构造

//私有方法
private:	
	void set_name(std::wstring str) { name = str; }
	void set_price(std::wstring str) { price = std::stoi(str); }
	void set_photo_200x150(std::wstring str) { photo_200x150 = str; }
	void set_photo_100x75(std::wstring str) { photo_100x75 = str; }
	void set_photo_140x105(std::wstring str) { photo_140x105 = str; }
	void set_sale(std::wstring str) { sale = std::stoi(str); }
	void set_repertory(std::wstring str) { repertory = std::stoi(str); }
	void set_bookstoreId(std::wstring str) { bookstoreId = std::stoi(str); }
	std::wstring get_name() const { return name; };
	std::wstring get_photo_200x150() const { return photo_200x150; };
	std::wstring get_photo_100x75() const { return photo_100x75; };
	std::wstring get_photo_140x105() const { return photo_140x105; };
	std::wstring get_price()const { return std::to_wstring(price); };
	std::wstring get_sale() const { return std::to_wstring(sale); };
	std::wstring get_repertory() const { return std::to_wstring(repertory); };
	std::wstring get_bookstoreId() const { return std::to_wstring(bookstoreId); };

//公有数据
public:	
	std::wstring name;								//名字
	std::wstring photo_200x150;						//图片位置 高150 x 宽200
	std::wstring photo_100x75;						//图片位置 高75 x 宽100
	std::wstring photo_140x105;						//图片位置 高105 x 宽140
	int price = -1;									//价格
	int bookstoreId = 0;							//商家id
	mutable int repertory = 0;						//库存
	mutable int sale = 0;							//销量
};


/**********************************   Bookstore  书店 **********************************/
class Bookstore {

//友元
	friend class BookstoreAdministrator;			//给书店管理类 友元
	friend class Read<Bookstore>;					//读取txt
	friend class Write<Bookstore>;					//写txt

//公有函数
public:
	Bookstore() noexcept {};												//默认构造
	virtual ~Bookstore() noexcept = default;								//析构函数
	Bookstore(const Bookstore& bookstore) noexcept = default;				//复制构造函数
	Bookstore& operator = (const Bookstore& bookstore) noexcept = default;	//赋值函数

//私有函数
private:
	explicit Bookstore(std::wifstream& file);		//构造函数 打开一个文件读取其中信息
	void set_name(std::wstring str) { name = str; }
	void set_bookPosition(std::wstring str) { bookPosition = str; }
	void set_id(std::wstring str) { id = std::stoi(str); }
	void set_portrait(std::wstring str) { portrait = str; }
	std::wstring get_name()  const { return name; }
	std::wstring get_bookPosition()const { return bookPosition; }
	std::wstring get_id()const { return std::to_wstring(id); }
	std::wstring get_portrait()const { return portrait; }
	
//保护函数
protected:
	void read(std::wifstream& file) { Read<Bookstore>::Fun(*this, file); }			//read
	void write(std::wofstream& file) const { Write<Bookstore>::Fun(*this, file); }	//write


//公有数据
public:	
	std::wstring name;								//书店名
	std::set<Book> books;							//图书集合
	std::wstring portrait;							//头像
	long long income = 0;							//收入
	int id = 0;										//书店id
	int booksCount = 0;								//图书数量
	int sale = 0;									//销量

//保护数据
protected:
public:
	std::wstring bookPosition;						//图书文件位置
};

/**********************************   Bookstore  书店   内联函数  ***********************/
/*读文件 构造函数 */
inline Bookstore::Bookstore(std::wifstream& file) {
	this->read(file);										//完成构造书店类
	std::wifstream filebooks(bookPosition);					//打开图书集合文件
	if (filebooks) {										//打开失败结束
		while (!filebooks.eof()) {							//一直读取
			Book book;
			book.bookstoreId = id;							//获取书店的id
			book.read(filebooks);							//调用book自己的read初始化自己
			if (book.name == L"")
				continue;
			books.emplace(book);							//存进数组
			booksCount++;
			sale += book.sale;
			income += static_cast<long long>(book.sale) * book.price;
		}
	}
};

/***************************        Shopping    购物类        **********************/
class Shopping;	//提前声明 获取友元

//购物类数据结构
struct ShoppingData {

//友元
	friend class ::Shopping;

//公有数据
public:
	Book book;															//书
	int count = 0;														//购买数量
	bool flag = false;													//结算标志

//私有数据
private:
	std::map<Book, std::list<ShoppingData>::iterator>::iterator p;		//list指向map的指针
};


//购物类
class Shopping {

//友元
	friend BookstoreAdministrator;										//给书店管理类 友元
	friend SingletonPattern<Shopping>;									//单例

//私有方法
private:
	inline Shopping();													//
	inline ~Shopping();													//
	inline int& operator[](Book& book);									//
	inline void eraser(Book& book);										//

	size_t size() { return mp.size(); }									//size
	std::list<ShoppingData>::iterator end() { return li.end(); }		//begin
	std::list<ShoppingData>::iterator begin() { return li.begin(); }	//end
	bool find(Book book) { return mp.find(book) != mp.end(); }			//find

//私有数据
private:
	std::map<Book, std::list<ShoppingData>::iterator> mp;				//map
	std::list<ShoppingData> li;											//list
	constexpr static char cs_position[] = "books\\Shopping.txt";		//缓存文件
};


/***************************        Shopping    购物类    内联函数    ********************/
/*添加 和 修改数量的接口*/
int& Shopping::operator[](Book& book) {
	if (mp.find(book) == mp.end()) {
		ShoppingData da;
		da.book = book;
		li.push_back(std::move(da));				//构造一个对象
		auto it = --li.end();						//获取链表迭代器
		mp[book] = it;								//哈希指向链表
		li.back().p = mp.find(book);				//链表指向哈希
	}
	return mp[book]->count;
}

/*从购物车删除*/
void Shopping::eraser(Book& book) {
	if (mp.find(book) != mp.end()) {
		std::list<ShoppingData>::iterator it = mp[book];	//备份链表指针
		mp.erase(book);										//先删除哈希表
		li.erase(it);										//再删除链表 
	}
}

/*默认初始化*/
Shopping::Shopping() {
	std::wifstream file(cs_position);					//打开文件
	while (!file.eof()) {
		Book book;
		book.read(file);							//读取购物车缓存信息
		if (book.name != L"")
			operator[](book);						//添加到购物车
	}
}

/*析构函数*/
Shopping::~Shopping() {
	std::wofstream file(cs_position);				//打开文件
	for (auto& it : li) {
		it.book.write(file);						//购物车内容存进去
	}
}


/*****************************   BookstoreAdministrator  书店管理    ******************/
class BookstoreAdministrator {

//友元
	friend class SingletonPattern<BookstoreAdministrator>;
//公有方法
public:
	//购物车相关 函数
	int& ShoppingBookCount(Book& book) { return shopping[book]; }
	void ShoppingEraser(Book& book) { shopping.eraser(book); }
	int ShoppingSize() { return static_cast<int> (shopping.size()); }
	std::list<ShoppingData>::iterator ShoppingEnd() { return shopping.end(); }
	std::list<ShoppingData>::iterator ShoppingBegin() { return shopping.begin(); }
	bool ShoppingFind(Book book) { return shopping.find(book); }


	inline const Book& operator()(Book book);					//查找功能 图书是否存在
	inline const Book& find(Book book);							//查找功能 图书是否存在	
	int BooksFind(Book& book1,Book& book2, std::vector<Book>& bks, 
		int x = -1, int y = 0b0111'1111'1111'1111);				//查找图书并将结果加入传入的数组

//私有函数
private:
	BookstoreAdministrator();
	inline ~BookstoreAdministrator();
	inline void shoppingFlush();								//刷新购物车

//公有数据
public:
	std::unordered_map<int, Bookstore> bookstores;				//书店表
	std::vector<Book> books;									//图书表
	constexpr static int cs_shoppingMax = 20;					//购物车最大量

//私有数据
private:
	Shopping& shopping;											//购物车
	constexpr static char cs_position[] = "books\\Bookstores.txt";
	constexpr static char cs_upperShopping[] = "books\\UpperShopping.txt";	//上次购物文件
	
};


/***********************   BookstoreAdministrator  书店管理   内联函数 ******************/


/*析构 摧毁购物车*/ 
BookstoreAdministrator::~BookstoreAdministrator() { SingletonPattern<Shopping>::Destroy(); }

/*查找功能 图书是否存在 版本1*/
const Book& BookstoreAdministrator::operator()(Book book) {
	return *bookstores[book.bookstoreId].books.find(book);	//
}

/*查找功能 图书是否存在 版本2*/
const Book& BookstoreAdministrator::find(Book book) {
	return *bookstores[book.bookstoreId].books.find(book);	//
}

/*刷新购物车数据*/
void BookstoreAdministrator::shoppingFlush() {
	for (auto& it : shopping) {
		it.book.repertory = find(it.book).repertory;
		it.book.sale = find(it.book).sale;
	}
}


#endif

