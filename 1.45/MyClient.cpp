#include"MyClient.h"

/*******************    ShoppingRecord           购物记录  数据    ******************/
Read<ShoppingRecordDate>::Hash_type Read<ShoppingRecordDate>::Hash = {
	{std::wstring(L"书名"),&ShoppingRecordDate::set_name},
	{std::wstring(L"商家"),&ShoppingRecordDate::set_bookstoreId},
	{std::wstring(L"店名"),&ShoppingRecordDate::set_bookstoreName},
	{std::wstring(L"顾客"),&ShoppingRecordDate::set_consumerId},
	{std::wstring(L"数量"),&ShoppingRecordDate::set_quantity},
	{std::wstring(L"价格"),&ShoppingRecordDate::set_sale},
	{std::wstring(L"时间"),&ShoppingRecordDate::set_time}
};

Write<ShoppingRecordDate>::Hash_type Write<ShoppingRecordDate>::Hash = {
	{std::wstring(L"书名"),&ShoppingRecordDate::get_name},
	{std::wstring(L"商家"),&ShoppingRecordDate::get_bookstoreId},
	{std::wstring(L"店名"),&ShoppingRecordDate::get_bookstoreName},
	{std::wstring(L"顾客"),&ShoppingRecordDate::get_consumerId},
	{std::wstring(L"数量"),&ShoppingRecordDate::get_quantity},
	{std::wstring(L"价格"),&ShoppingRecordDate::get_sale},
	{std::wstring(L"时间"),&ShoppingRecordDate::get_time}
};


/************************   Consumer          顾客 消费者      ************************/
Read<Consumer>::Hash_type Read<Consumer>::Hash = {
	{std::wstring(L"顾客"),&Consumer::set_name},
	{std::wstring(L"ID"),&Consumer::set_id},
	{std::wstring(L"头像"),&Consumer::set_portrait},
	{std::wstring(L"余额"),&Consumer::set_balance}
};

Write<Consumer>::Hash_type Write<Consumer>::Hash = {
	{std::wstring(L"顾客"),&Consumer::get_name},
	{std::wstring(L"ID"),&Consumer::get_id},
	{std::wstring(L"头像"),&Consumer::get_portrait},
	{std::wstring(L"余额"),&Consumer::get_balance}
};


/********************     ConsumerAdministrator       顾客管理       ********************/
Read<ConsumerAdministrator::PasswordData>::Hash_type 
	Read<ConsumerAdministrator::PasswordData>::Hash = {
	{std::wstring(L"顾客"),&ConsumerAdministrator::PasswordData::set_name},
	{std::wstring(L"数据"),&ConsumerAdministrator::PasswordData::set_data}
};

Write<ConsumerAdministrator::PasswordData>::Hash_type
	Write<ConsumerAdministrator::PasswordData>::Hash = {
	{std::wstring(L"顾客"),&ConsumerAdministrator::PasswordData::get_name},
	{std::wstring(L"数据"),&ConsumerAdministrator::PasswordData::get_data}
};


Read<ConsumerAdministrator::ConsumerData>::Hash_type
Read<ConsumerAdministrator::ConsumerData>::Hash = {
{std::wstring(L"顾客"),&ConsumerAdministrator::ConsumerData::set_name},
{std::wstring(L"头像"),&ConsumerAdministrator::ConsumerData::set_head},
{std::wstring(L"ID"),& ConsumerAdministrator::ConsumerData::set_id},
{std::wstring(L"余额"),&ConsumerAdministrator::ConsumerData::set_balance}
};

Write<ConsumerAdministrator::ConsumerData>::Hash_type
Write<ConsumerAdministrator::ConsumerData>::Hash = {
{std::wstring(L"顾客"),&ConsumerAdministrator::ConsumerData::get_name},
{std::wstring(L"头像"),&ConsumerAdministrator::ConsumerData::get_head},
{std::wstring(L"ID"),& ConsumerAdministrator::ConsumerData::get_id},
{std::wstring(L"余额"),&ConsumerAdministrator::ConsumerData::get_balance}
};