#include"MyPage.h"

/**************************  PageAdministrator  页面管理类    ******************************/
BookstoreAdministrator& PageAdministrator::bookstoreAdministrator
			= *SingletonPattern<BookstoreAdministrator>::Get();		//图书管理类
ConsumerAdministrator& PageAdministrator::consumerAdministrator
			= *SingletonPattern<ConsumerAdministrator>::Get();		//用户管理类
std::shared_ptr<Page> PageAdministrator::mainPage;					//主页面指针
std::shared_ptr<Page> PageAdministrator::mainAdministratorPage;		//管理员主页面指针
int PageAdministrator::bookCount;									//当前书计数


/*析构构造*/
PageAdministrator::~PageAdministrator() {
	SingletonPattern<BookstoreAdministrator>::Destroy();
	SingletonPattern<ConsumerAdministrator>::Destroy();
}

/*默认构造 用户*/
PageAdministrator::PageAdministrator() : PageAdministratorBase() {
	initializeSetPicture();							//设置画面显示属性
	if (atPresentPage == nullptr) {					//如果当前页面指针为空 就产生一个广告页
		atPresentPage.reset(new LoadingPage);
	}
	if (mainPage == nullptr) {
		mainPage.reset(new MainPage1_2);
	}
}

/*管理员*/
PageAdministrator::PageAdministrator(int k) : PageAdministratorBase() {
	initializeSetPicture();							//设置画面显示属性
	if (atPresentPage == nullptr) {					//如果当前页面指针为空 就产生一个广告页
		atPresentPage.reset(new LoadingPage);
	}
	if (mainAdministratorPage == nullptr) {
		mainAdministratorPage.reset(new AdministratorMainPage);
	}
	if (mainPage == nullptr) {
		mainPage.reset(new MainPage1_2);
	}
}

//显示加载页
void PageAdministrator::Loading() {
	LoadingPage* p = dynamic_cast<LoadingPage*>(atPresentPage.get());
	if (p != nullptr) {
		p->data.wstrs.push_back(L"正在加载程序需要的数据");
		p->data.wstrs.push_back(L"1.正在加载商户数据...");
		p->data.wstrs.push_back(L"2.正在加载图书数据...");
		p->data.wstrs.push_back(L"3.正在结算上次购物...");
		p->data.wstrs.push_back(L"4.正在加载购物车数据...");
		p->data.wstrs.push_back(L"5.正在加载用户信息...");
		consumerAdministrator.init();
		p->data.wstrs.push_back(L"6.全部加载完成，即将跳转登入。");
	}
	atPresentPage.reset(new LoginPage);								//跳转登入
}


/*****************************     MainPage1_2      主页类      ******************************/
std::vector<Book> MainPage1_2::books;
bool MainPage1_2::FlagIniBook = false;
Book MainPage1_2::b1, MainPage1_2::b2;

/*将图书数组里面的数据导入页面*/
void MainPage1_2::AddBooks() {
	int temp = 0;
	if (bookCount >= books.size()) {
		temp = bookstoreAdministrator.BooksFind(b1, b2, books, bookCount, 20);}
	else { temp = 20; }							//缓存里面已经有了，不加载
	if (temp < 20) { FlagAddBook = false;}		//设置标志 能否继续添加图书

	bookCountMax = static_cast<int>(books.size());
	computeDynamicCountMax(220);
	constexpr int offw = 155;
	constexpr int offh = 220;
	constexpr int ph = 90;
	constexpr int pw = 105;
	for (int j = bookCount / 4, k = 0; k < 5 && bookCount < bookCountMax; j++,k++) {
		for (int i = 0; i < 4 && bookCount < bookCountMax; i++, bookCount++) {
			{	//图片
				std::shared_ptr<Click> ckP(new Click);
				ckP->x1 = pw + i * offw;
				ckP->x2 = pw + 105 + i * offw;
				ckP->y1 = ph + j * offh;
				ckP->y2 = ph + 140 + j * offh;
				auto& temp = books[bookCount];
				ckP->fun = ([&temp]()->std::shared_ptr<Page> {
					return std::shared_ptr<Page>(new BookPage(temp)); });
				ckP->position = books[bookCount].photo_140x105;
				dynamicClicks.push_back(ckP);
			}
			{	//显示名称
				std::shared_ptr<NoClick> nckP(new NoClick);
				nckP->x1 = pw - 25 + i * offw;
				nckP->x2 = pw + 130 + i * offw;
				nckP->y1 = ph + 140 + j * offh;
				nckP->y2 = ph + 170 + j * offh;
				nckP->high = 18;
				nckP->words = books[bookCount].name;
				dynamicNoClicks.push_back(nckP);
			}
			{	//显示价格
				std::shared_ptr<NoClick> nckP(new NoClick);
				nckP->x1 = pw + i * offw;
				nckP->x2 = pw + 105 + i * offw;
				nckP->y1 = ph + 170 + j * offh;
				nckP->y2 = ph + 200 + j * offh;
				nckP->high = 18;
				nckP->words = L"￥" + std::to_wstring(books[bookCount].price);
				dynamicNoClicks.push_back(nckP);
			}

		}//for
	}//for
	{	//加载更多功能
		std::shared_ptr<ClickHandover> ckP(new ClickHandover);
		ckP->x1 = 100;
		ckP->x2 = 700;
		ckP->y1 = 480 - dynamicCountMax - 60;
		ckP->y2 = 480 - dynamicCountMax - 20;
		ckP->high = 24;
		ckP->handoverWords.resize(2);
		ckP->handoverCountMax = 2;
		ckP->handoverWords[1] = L"加载更多";
		ckP->handoverWords[0] = L"已经全部加载";
		ckP->handoverCount = 1;
		auto& tempClickHandover = *ckP;
		ckP->fun = [&tempClickHandover, this]()->std::shared_ptr<Page> {
			tempClickHandover.handoverCount = MainPage1_2::FlagAddBook;
			if (tempClickHandover.handoverCount) {
				std::shared_ptr<MainPage1_2> ckP(new MainPage1_2(*this));
				if (ckP->dynamicClicks.size() > 1) { 
					ckP->dynamicClicks.pop_back(); }	//移除 旧“加载更多”
				ckP->AddBooks();						//添加新的书和 “加载更多”
				PageAdministrator::
				PageAdministrator::FlagNORollback = true;	//不可回退
				return ckP;
			}
			return nullptr;
		};
		dynamicClicks.push_back(ckP);
	}
}

/*默认构造*/
MainPage1_2::MainPage1_2() {
	books.clear();										//图书数组
	bookCount = 0;										//图书计数
	bookCountMax = 0;
	FlagAddBook = true;

	PageFun::AddExitkFun(*this);					//退出
	PageFun::AddVersionFun1_4(*this);				//版本
	PageFun::AccessAdministratorPage(*this);		//管理员模式
	//PageFun::AccessShoppingRecordPage(*this);		//购物记录
	PageFun::AccessWalletPage(*this);				//钱包
	//PageFun::AccessSetPage(*this);				//进入设置页面
	PageFun::AccessLogoPage(*this);					//logo
	PageFun::AccessShoppingPage(*this);				//购物车

	if (!FlagIniBook) {
		RestoreFindBook();}							//初始化查找书 搜索模式没有这个初始化

	AddSearchFun();									//搜索函数

	AddBooks();										//从数组添加书
}

void MainPage1_2::AddSearchFun() {
	ImportDataBase* nameP = nullptr;
	ImportDataBase* price1P = nullptr;
	ImportDataBase* price2P = nullptr;
	ImportDataBase* sale1P = nullptr;
	ImportDataBase* sale2P = nullptr;
	ImportDataBase* repertory1P = nullptr;

	constexpr int w1 = 700;
	constexpr int w2 = 740;
	constexpr int w3 = 755;
	constexpr int w4 = 800;

	constexpr int h1 = 45;		//字
	constexpr int h2 = 75;
	constexpr int hz1 = 50;		//输入
	constexpr int hz2 = 70;
	constexpr int l1 = 100;		//价格150
	constexpr int r1 = 255;
	constexpr int l2 = 260;		//销量180
	constexpr int r2 = 415;
	constexpr int l3 = 420;		//库存120
	constexpr int r3 = 540;


	{	//输入框
		std::shared_ptr<ClickImportr> ckP(new ClickImportr(MainPage1_2::b1.name));
		ckP->x1 = 100;
		ckP->x2 = 630;
		ckP->y1 = 10;
		ckP->y2 = 40;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		nameP = &ckP->importData;
		ckP->fun = ([nameP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(nameP);		//绑定输入
			return nullptr; });
		dynamicClicks.push_back(ckP);
	}
	{	//价格区间
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = l1;
		nckP->x2 = l1 + 60;
		nckP->y1 = h1;
		nckP->y2 = h2;
		nckP->high = 20;
		nckP->words = L"价格：";
		dynamicNoClicks.push_back(nckP);
	}
	{	// - 
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = r1 - 55;
		nckP->x2 = r1 - 40;
		nckP->y1 = hz1;
		nckP->y2 = hz2;
		nckP->high = 16;
		nckP->words = L"-";
		dynamicNoClicks.push_back(nckP);
	}
	{	//价格 小
		std::shared_ptr<ClickImportrNumber> ckP(new ClickImportrNumber(MainPage1_2::b1.price));
		ckP->x1 = l1 + 60;
		ckP->x2 = l1 + 115;
		ckP->y1 = hz1;
		ckP->y2 = hz2;
		ckP->high = 16;
		ckP->importData.maxCount = 4;
		price1P = &ckP->importData;
		ckP->fun = ([price1P]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(price1P);		//绑定输入
			return nullptr; });
		dynamicClicks.push_back(ckP);
	}
	{	//价格 大
		std::shared_ptr<ClickImportrNumber> ckP(new ClickImportrNumber(MainPage1_2::b2.price));
		ckP->x1 = r1 - 40;
		ckP->x2 = r1;
		ckP->y1 = hz1;
		ckP->y2 = hz2;
		ckP->high = 16;
		ckP->importData.maxCount = 4;
		price2P = &ckP->importData;
		ckP->fun = ([price2P]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(price2P);		//绑定输入
			return nullptr; });
		dynamicClicks.push_back(ckP);
	}
	{	//销量区间
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = l2;
		nckP->x2 = l2 + 60;
		nckP->y1 = h1;
		nckP->y2 = h2;
		nckP->high = 20;
		nckP->words = L"销量：";
		dynamicNoClicks.push_back(nckP);
	}
	{	// - 
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = r2 - 55;
		nckP->x2 = r2 - 40;
		nckP->y1 = hz1;
		nckP->y2 = hz2;
		nckP->high = 16;
		nckP->words = L"-";
		dynamicNoClicks.push_back(nckP);
	}
	{	//销量 小
		std::shared_ptr<ClickImportrNumber> ckP(new ClickImportrNumber(MainPage1_2::b1.sale));
		ckP->x1 = l2 + 60;
		ckP->x2 = l2 + 115;
		ckP->y1 = hz1;
		ckP->y2 = hz2;
		ckP->high = 16;
		ckP->importData.maxCount = 4;
		sale1P = &ckP->importData;
		ckP->fun = ([sale1P]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(sale1P);		//绑定输入
			return nullptr; });
		dynamicClicks.push_back(ckP);
	}
	{	//销量 大
		std::shared_ptr<ClickImportrNumber> ckP(new ClickImportrNumber(MainPage1_2::b2.sale));
		ckP->x1 = r2 - 40;
		ckP->x2 = r2;
		ckP->y1 = hz1;
		ckP->y2 = hz2;
		ckP->high = 16;
		ckP->importData.maxCount = 4;
		sale2P = &ckP->importData;
		ckP->fun = ([sale2P]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(sale2P);		//绑定输入
			return nullptr; });
		dynamicClicks.push_back(ckP);
	}
	{	//库存
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = l3;
		nckP->x2 = l3 + 80;
		nckP->y1 = h1;
		nckP->y2 = h2;
		nckP->high = 20;
		nckP->words = L"库存：>";
		dynamicNoClicks.push_back(nckP);
	}
	{	//库存 小
		std::shared_ptr<ClickImportrNumber> ckP(new ClickImportrNumber(MainPage1_2::b1.repertory));
		ckP->x1 = r3 - 40;
		ckP->x2 = r3;
		ckP->y1 = hz1;
		ckP->y2 = hz2;
		ckP->high = 16;
		ckP->importData.maxCount = 4;
		repertory1P = &ckP->importData;
		ckP->fun = ([repertory1P]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(repertory1P);		//绑定输入
			return nullptr; });
		dynamicClicks.push_back(ckP);
	}
	{
		//搜索
		std::shared_ptr<Click> ckP2(new Click);
		ckP2->x1 = 635;
		ckP2->x2 = 700;
		ckP2->y1 = 10;
		ckP2->y2 = 75;
		ckP2->high = 24;
		ckP2->position = L"pages\\ico1.jpg";
		ckP2->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;		//不可回退
			std::shared_ptr<Page> p(new MainPage1_2);
			return p; };
		dynamicClicks.push_back(ckP2);

	}
	{
		//搜索
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 710;
		ckP->x2 = 800;
		ckP->y1 = 65;
		ckP->y2 = 85;
		ckP->high = 18;
		ckP->words = L"重置搜索";
		ckP->fun = []()->std::shared_ptr<Page> {
			MainPage1_2::RestoreFindBook();
			std::shared_ptr<Page> p(new MainPage1_2);
			return p; };
		clicks.push_back(ckP);

	}
	{
		//搜索
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 740;
		ckP->x2 = 781;
		ckP->y1 = 30;
		ckP->y2 = 60;
		ckP->high = 24;
		ckP->position = L"pages\\ico2.jpg";
		ckP->fun = []()->std::shared_ptr<Page> {
			MainPage1_2::RestoreFindBook();
			std::shared_ptr<Page> p(new MainPage1_2);
			return p; };
		clicks.push_back(ckP);

	}
}


/***********************     AdministratorMainPage      管理员主页类      ********************/
/*构造函数 管理员*/
AdministratorMainPage::AdministratorMainPage() {
	PageFun::AddExitkFun(*this);				//退出
	{	//1.查看商户
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 0;
		ckP->x2 = 150;
		ckP->y1 = 10;
		ckP->y2 = 40;
		ckP->fun = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new AdministratorBookstore); };
		ckP->words = L"1.查看商户";
		clicks.push_back(ckP);
	}
	{	//2.查看图书
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 0;
		ckP->x2 = 150;
		ckP->y1 = 50;
		ckP->y2 = 80;
		ckP->fun = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new AdministratorBook); };
		ckP->words = L"2.查看图书";
		clicks.push_back(ckP);
	}
	{	//3.查看图书
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 0;
		ckP->x2 = 150;
		ckP->y1 = 90;
		ckP->y2 = 120;
		ckP->fun = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new AdministratorAddBookstore); };
		ckP->words = L"3.添加商户";
		clicks.push_back(ckP);
	}
	{	//3.查看图书
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 0;
		ckP->x2 = 150;
		ckP->y1 = 130;
		ckP->y2 = 160;
		ckP->fun = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new AdministratorAddBook); };
		ckP->words = L"4.添加图书";
		clicks.push_back(ckP);
	}

	{//进入用户模式
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 600;
		ckP->x2 = 735;
		ckP->y1 = 445;
		ckP->y2 = 475;
		ckP->fun = []()->std::shared_ptr<Page> {
			return PageAdministrator::mainPage; };
		ckP->words = L"用户模式";
		clicks.push_back(ckP);
	}
}


/******************************     BookPage     书详细信息页面类      ************************/
/*书详细页构造函数*/
BookPage::BookPage(Book& book) : book(book) {
	{	//书的图标
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 100;
		nckP->x2 = 250;
		nckP->y1 = 130;
		nckP->y2 = 330;
		nckP->position = book.photo_200x150;
		noClicks.push_back(nckP);
	}
	{	//显示名称
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 70;
		nckP->x2 = 280;
		nckP->y1 = 330;
		nckP->y2 = 370;
		nckP->words = book.name;
		noClicks.push_back(nckP);
	}
	{	//显示价格
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 100;
		nckP->x2 = 250;
		nckP->y1 = 370;
		nckP->y2 = 390;
		nckP->words = L"￥" + std::to_wstring(book.price);
		noClicks.push_back(nckP);
	}
	{	//显示销量
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 100;
		nckP->x2 = 250;
		nckP->y1 = 400;
		nckP->y2 = 420;
		nckP->words = L"销量：" + std::to_wstring(book.sale);
		noClicks.push_back(nckP);
	}
	{	//显示库存
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 100;
		nckP->x2 = 250;
		nckP->y1 = 430;
		nckP->y2 = 450;
		nckP->words = L"库存：" + std::to_wstring(book.repertory);
		noClicks.push_back(nckP);
	}
	{	//商家名称
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 100;
		nckP->x2 = 250;
		nckP->y1 = 10;
		nckP->y2 = 30;
		nckP->words = L"商家：" + PageAdministrator::
			bookstoreAdministrator.bookstores[book.bookstoreId].name;
		noClicks.push_back(nckP);
	}
	{	//商家头像
		std::shared_ptr<NoClick> cknP(new NoClickPortrait);
		cknP->x1 = 2;
		cknP->x2 = 92;
		cknP->y1 = 2;
		cknP->y2 = 92;
		cknP->position = PageAdministrator::
			bookstoreAdministrator.bookstores[book.bookstoreId].portrait;
		noClicks.push_back(cknP);
	}
	{	//添加到购物车
		std::shared_ptr<ClickHandover> ckP(new ClickHandover);
		ckP->x1 = 700;
		ckP->x2 = 790;
		ckP->y1 = 380;
		ckP->y2 = 465;
		ckP->handoverPosition.push_back(L"pages\\ico3.jpeg");
		ckP->handoverPosition.push_back(L"pages\\ico4.jpg");
		ckP->handoverCount = bookstoreAdministrator.ShoppingFind(book);
		ckP->handoverCountMax = 2;
		int& temp  = ckP->handoverCount;
		Book& tempBook = this->book;
		ckP->fun = [&tempBook,&temp]()->std::shared_ptr<Page>{
			if (temp) {
				PageAdministrator::bookstoreAdministrator.ShoppingEraser(tempBook);
				temp ^= 1;
			}
			else if(PageAdministrator::bookstoreAdministrator.ShoppingSize() 
				< PageAdministrator::bookstoreAdministrator.cs_shoppingMax
				&& tempBook.repertory > 0) {			//库存大于0 且购物车容量足够
				PageAdministrator::bookstoreAdministrator.ShoppingBookCount(tempBook)++;
				temp ^= 1;
			}
			PageAdministrator::FlagDynamicFlush = true;		//强制刷新
			PageAdministrator::FlagNORollback = true;		//不可回退
			return nullptr;
		};
		clicks.push_back(ckP);

	}
	{	//购物车已满
		std::shared_ptr<NoClickDynamicValue> nckP(new NoClickDynamicValue);
		nckP->x1 = 660;
		nckP->x2 = 800;
		nckP->y1 = 350;
		nckP->y2 = 380;
		nckP->dynamicWordsFun = []()->std::wstring {
			return PageAdministrator::bookstoreAdministrator.ShoppingSize()
					< PageAdministrator::bookstoreAdministrator.cs_shoppingMax
					? L" " : L"购物车已满！";
		};
		noClicks.push_back(nckP);
	}
	{	//库存不足
		std::shared_ptr<NoClickDynamicValue> nckP(new NoClickDynamicValue);
		nckP->x1 = 680;
		nckP->x2 = 800;
		nckP->y1 = 320;
		nckP->y2 = 350;
		Book& tempBook = this->book;
		nckP->dynamicWordsFun = [&tempBook]()->std::wstring {
			return tempBook.repertory > 0 ? L" " : L"库存不足！";
		};
		noClicks.push_back(nckP);
	}

	PageFun::AddRollbackFun(*this);						//添加返回功能
}


/*****************************    ShoppingPage  购物车页面      ******************************/
/*计算最大下滑量*/
void ShoppingPage::computeDynamicCountMax() {
	dynamicCountMax = bookstoreAdministrator.ShoppingSize() > 3
		? -150 * (bookstoreAdministrator.ShoppingSize() - 3) : 0;
}

/*构造函数*/
ShoppingPage::ShoppingPage() {
	PageFun::AddRollbackFun(*this);							//添加返回功能
	computeDynamicCountMax();								//计算最大滑动

	std::vector<std::function <std::shared_ptr<Page>()>>* funsFalseP = nullptr;
	std::vector<std::function <std::shared_ptr<Page>()>>* funsTrueP = nullptr;
	{	//全选 false
		std::shared_ptr<ClickS> ckP(new ClickS);
		ckP->x1 = 700;
		ckP->x2 = 800;
		ckP->y1 = 80;
		ckP->y2 = 110;
		ckP->high = 24;
		ckP->words = L"全部选中";
		funsFalseP = &ckP->funs;
		clicks.push_back(ckP);
	}
	{	//重选 true
		std::shared_ptr<ClickS> ckP(new ClickS);
		ckP->x1 = 700;
		ckP->x2 = 800;
		ckP->y1 = 120;
		ckP->y2 = 150;
		ckP->high = 24;
		ckP->words = L"重新选择";
		funsTrueP = &ckP->funs;
		clicks.push_back(ckP);
	}


	int k = 0;
	constexpr int off = 150;								//距离 和上面函数150一起改
	constexpr int hg1 = 55;									//购买量的高度
	constexpr int hg2 = 85;
	for (auto bookdataP = bookstoreAdministrator.ShoppingBegin(),bookdataEndP 
		= bookstoreAdministrator.ShoppingEnd();bookdataP != bookdataEndP; bookdataP++)
	{
		data.push_back(std::make_pair(bookdataP->book.price, bookdataP->count));
		flag.push_back(bookdataP->flag);

		{	//图标
			std::shared_ptr<Click> ckP(new Click);
			ckP->x1 = 100;
			ckP->x2 = 175;
			ckP->y1 = 30 + k * off;
			ckP->y2 = 130 + k * off;
			ckP->position = bookdataP->book.photo_100x75;
			ckP->fun = []()->std::shared_ptr<Page> {
				return std::shared_ptr<Page>(new ShoppingPage);
			};			//需要改
			dynamicClicks.push_back(ckP);
		}
		{	//单价￥
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = 355;
			nckP->x2 = 445;
			nckP->y1 = 40 + k * off;
			nckP->y2 = 70 + k * off;
			nckP->high = 20;
			nckP->words = L"￥" + std::to_wstring(data[k].first);
			dynamicNoClicks.push_back(nckP);
		}
		{	//总计￥
			std::shared_ptr<NoClickDynamicValue> nckP(new NoClickDynamicValue);
			nckP->x1 = 355;
			nckP->x2 = 445;
			nckP->y1 = 80 + k * off;
			nckP->y2 = 110 + k * off;
			nckP->high = 20;
			nckP->words = L"￥" + std::to_wstring(bookdataP->book.price);
			nckP->dynamicWordsFun = [this,k]()->std::wstring {
				return L"￥" + std::to_wstring(this->data[k].first * this->data[k].second);
			};
			dynamicNoClicks.push_back(nckP);
		}

		{	//购买量
			std::shared_ptr<NoClickDynamicValue> nckP(new NoClickDynamicValue);
			nckP->x1 = 505;
			nckP->x2 = 605;
			nckP->y1 = hg1 + k * off;
			nckP->y2 = hg2 + k * off;
			auto temp = bookdataP->book.repertory;
			nckP->dynamicWordsFun = [this, k,temp]()->std::wstring {
				return std::to_wstring(this->data[k].second) + L"/"
					+ std::to_wstring(temp);
			};
			dynamicNoClicks.push_back(nckP);
		}
		{	// "+"
			std::shared_ptr<Click> ckP(new Click);
			ckP->x1 = 605;
			ckP->x2 = 635;
			ckP->y1 = hg1 + k * off;
			ckP->y2 = hg2 + k * off;
			ckP->high = 40;
			ckP->words = L"+";
			ckP->fun = [bookdataP,this,k]()->std::shared_ptr<Page> {
				if (bookdataP->count < bookdataP->book.repertory) {
					bookdataP->count++;							//库存还够
					this->data[k].second++;						//修改页面的数据
				}
				PageAdministrator::FlagNORollback = true;		//不可回退
				return nullptr;
			};
			dynamicClicks.push_back(ckP);
		}
		{	// "-"
			std::shared_ptr<Click> ckP(new Click);
			ckP->x1 = 475;
			ckP->x2 = 505;
			ckP->y1 = hg1 + k * off;
			ckP->y2 = hg2 + k * off;
			ckP->high = 40;
			ckP->words = L"-";
			ckP->fun = [bookdataP, this,k]()->std::shared_ptr<Page> {
				if (bookdataP->count > 0) {
					bookdataP->count--;
					this->data[k].second--;
				}
				PageAdministrator::FlagNORollback = true;			//不可回退
				return nullptr;
			};
			dynamicClicks.push_back(ckP);
		}
		{	//书名
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = 210;
			nckP->x2 = 340;
			nckP->y1 = 40 + k * off;
			nckP->y2 = 70 + k * off;
			nckP->high = 20;
			nckP->words = bookdataP->book.name;
			dynamicNoClicks.push_back(nckP);
		}
		{	//选中
			std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
			ckP->x1 = 15;
			ckP->x2 = 90;
			ckP->y1 = 30 + k * off;
			ckP->y2 = 105 + k * off;
			ckP->flag = flag[k];
			ckP->position = L"pages\\ico5.jpg";
			ckP->positionTrue = L"pages\\ico6.jpg";
			auto tempP = ckP.get();
			ckP->funFalse =[bookdataP,tempP,this,k]()->std::shared_ptr<Page> {
				if (bookdataP->book.repertory > 0 ) {			//库存0 不可选中
					tempP->flag = true;							//修改显示标志
					bookdataP->flag = true;						//修改购物车选中标志
					this->flag[k] = true;
					bookdataP->count == 0 ? bookdataP->count = 1,this->data[k].second++ : 0;
				}
				PageAdministrator::FlagNORollback = true;		//不可回退
				return nullptr;
			};
			ckP->funTrue = [bookdataP, tempP,this,k]()->std::shared_ptr<Page> {
				tempP->flag = false;							//修改显示标志
				bookdataP->flag = false;						//修改购物车选中标志
				this->flag[k] = false;
				PageAdministrator::FlagNORollback = true;		//不可回退
				return nullptr;
			};
			dynamicClicks.push_back(ckP);
			funsFalseP->push_back(ckP->funFalse);				//全选
			funsTrueP->push_back(ckP->funTrue);				//重选
		}


		{	//删除
			std::shared_ptr<Click> ckP(new Click);
			ckP->x1 = 182;
			ckP->x2 = 200;
			ckP->y1 = 22 + k * off;
			ckP->y2 = 40 + k * off;
			ckP->position = L"pages//ico7.jpg";
			ckP->fun = [bookdataP]()->std::shared_ptr<Page> {
				PageAdministrator::bookstoreAdministrator.ShoppingEraser(bookdataP->book);
				std::shared_ptr<ShoppingPage> tempP(new ShoppingPage);
				tempP->SetDynamicCount(PageAdministrator::dynamicCount);
				PageAdministrator::FlagNORollback = true;			//不可回退
				return tempP;
			};
			dynamicClicks.push_back(ckP);
		}
		k++;				//计数++
	}

	{	//总价
		std::shared_ptr<NoClickDynamicValue> nckP(new NoClickDynamicValue);
		nckP->x1 = 640;
		nckP->x2 = 800;
		nckP->y1 = 410;
		nckP->y2 = 440;
		nckP->high = 18;
		nckP->dynamicWordsFun = [this]()->std::wstring {
			int value = 0;
			int size = static_cast<int>(this->data.size());
			for (int i = 0; i < size; i++) {
				if (this->flag[i]) {
					value += this->data[i].first * this->data[i].second;
				}
			}
			return L"合计：￥" + std::to_wstring(value);
		};
		noClicks.push_back(nckP);
	}
	{	//去结算
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 640;
		ckP->x2 = 800;
		ckP->y1 = 440;
		ckP->y2 = 470;
		ckP->words = L"结算";
		ckP->fun = []()->std::shared_ptr<PaymentPage> {
			return std::shared_ptr<PaymentPage>(new PaymentPage);
		};
		clicks.push_back(ckP);
	}

}


/************************************  Thanks   致谢   ************************************/
/*read函数*/
Read<Thanks>::Hash_type Read<Thanks>::Hash = {
	{L"姓名",&Thanks::get_name},
	{L"图片",&Thanks::get_postion }
};


/********************************  ThanksPage   致谢页   ***************************/
std::vector<Thanks> ThanksPage::thanksArr;
/*构造函数*/
ThanksPage::ThanksPage() {
	if (thanksArr.size() == 0) {							//初始化数据
		std::wifstream file(postion);
		while (!file.eof()) {
			Thanks ts;
			ts.read(file);
			if (ts.name != L"") {
				thanksArr.push_back(ts);
			}
		}
	}

	PageFun::AddRollbackFun_Dynamic(*this);					//返回功能

	computeDynamicCountMax();								//计算最大滑动
	for (int i = 0; i < thanksArr.size();) {
		for (int j = 0; j < 4 && i < thanksArr.size(); j++, i++) {
			{	//图标
				std::shared_ptr<NoClickPortrait> nckP(new NoClickPortrait);
				nckP->x1 = 40 + j * 190;
				nckP->x2 = 190 + j * 190;
				nckP->y1 = 180 + i/4 * 210;
				nckP->y2 = 330 + i/4 * 210;
				nckP->position = thanksArr[i].postion;
				dynamicNoClicks.push_back(nckP);
			}
			{	//名字
				std::shared_ptr<NoClick> nckP(new NoClick);
				nckP->x1 = 40 + j * 190;
				nckP->x2 = 190 + j * 190;
				nckP->y1 = 330 + i/4 * 210;
				nckP->y2 = 360 + i/4 * 210;
				nckP->words = thanksArr[i].name;
				dynamicNoClicks.push_back(nckP);
			}
		}	//for
	}	//for
	{	// old logo
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 0;
		nckP->x2 = 160;
		nckP->y1 = 0;
		nckP->y2 = 120;
		nckP->position = L"pages\\ico11.jpg";
		dynamicNoClicks.push_back(nckP);
	}
	{	//
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 120;
		nckP->x2 = 800;
		nckP->y1 = 30;
		nckP->y2 = 60;
		nckP->words = L"致谢";
		dynamicNoClicks.push_back(nckP);
	}
	{	//
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 500;
		nckP->x2 = 800;
		nckP->y1 = 480 - dynamicCountMax - 45;
		nckP->y2 = 480 - dynamicCountMax - 12;
		nckP->words = L"玲芦（小助手） - 淘书1.1";
		dynamicNoClicks.push_back(nckP);
	}
	{	//
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 120;
		nckP->x2 = 800;
		nckP->y1 = 60;
		nckP->y2 = 90;
		nckP->words = L"“淘书”项目能够完成离，不开以下朋友的帮助，在此感谢！";
		dynamicNoClicks.push_back(nckP);
	}
	{	//
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 120;
		nckP->x2 = 800;
		nckP->y1 = 90;
		nckP->y2 = 120;
		nckP->words = L"其他提供过帮助的朋友会作为彩蛋在“淘书”中出现";
		dynamicNoClicks.push_back(nckP);
	}

}

/*计算最大下滑量*/
void ThanksPage::computeDynamicCountMax() {
	dynamicCountMax = static_cast<int>(thanksArr.size()) > 4 
		? -190 * (static_cast<int>(thanksArr.size()) - 4) : 0;
}


/*******************************       Logo      日志     ********************************/
Read<Logo>::Hash_type Read<Logo>::Hash = { 
	{L"日志", &Logo::set_diary} 
};


/*********************     LogoAdministrator      logo管理类      **********************/
std::vector<Logo> LogoAdministrator::logos;
int LogoAdministrator::count = 0;

/*read函数*/
Read<LogoAdministrator>::Hash_type Read<LogoAdministrator>::Hash = { 
	{L"数量", & LogoAdministrator::set_count}
};

/*logo管理构造函数*/
LogoAdministrator::LogoAdministrator() {
	if (count == 0) {
		std::wifstream file(postion);
		read(file);
		for (int i = 0; i < count; i++) {
			Logo lg;
			lg.read(file);
			logos.push_back(lg);
		}
	}
}


/*****************************     LogoPage      logo页    ******************************/
int LogoPage::count = 0;
int LogoPage::countMax = 0;

LogoPage::LogoPage() {				/*LogoPage构造函数*/
	PageFun::AddRollbackFun(*this);						//添加返回功能

	countMax = static_cast<int>(logoAdministrator.logos.size());
	{	//下一页
		std::shared_ptr<Click> ckP(new Click);
		Click ck;
		ckP->x1 = 720;
		ckP->x2 = 780;
		ckP->y1 = 210;
		ckP->y2 = 270;
		ckP->position = L"pages\\ico9.png";
		ckP->fun = []()->std::shared_ptr<Page> {
			if (LogoPage::count + 1 < LogoPage::countMax) {
				PageAdministrator::FlagNORollback = true;		//不可回退
				PageAdministrator::FlagDynamicFlush = true;
				LogoPage::count++;
				return nullptr;}
			else {return nullptr;}
		};
		clicks.push_back(ckP);

	}
	{	//上一页
		std::shared_ptr<Click> ckP(new Click);
		Click ck;
		ckP->x1 = 20;
		ckP->x2 = 80;
		ckP->y1 = 210;
		ckP->y2 = 270;
		ckP->position = L"pages\\ico10.png";
		ckP->fun = []()->std::shared_ptr<Page> {
			if (LogoPage::count > 0) {
				LogoPage::count--;
				PageAdministrator::FlagNORollback = true;		//不可回退
				PageAdministrator::FlagDynamicFlush = true;
				return nullptr;}
			else { return nullptr; }
		};
		clicks.push_back(ckP);

	}

	/*产生一个logo点击*/{	
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 0;
		ckP->x2 = 160;
		ckP->y1 = 0;
		ckP->y2 = 120;
		ckP->fun = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new ThanksPage); };
		ckP->position = L"pages\\ico11.jpg";
		clicks.push_back(ckP);
	}
}


/************************************  PaymentPage   支付页   ************************************/
/*计算最大下滑量*/
void PaymentPage::computeDynamicCountMax(int off, int k) {
	dynamicCountMax = k > 8 ? -off * (k - 8) : 0;
}

/*构造函数*/
PaymentPage::PaymentPage() {
	PageFun::AddRollbackFun(*this);						//添加返回功能

	int k = 0;
	constexpr int off = 40;
	constexpr int ph = 50;
	int paymentValue = 0;									//付款值
	{	//编号
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 20;
		nckP->x2 = 80;
		nckP->y1 = ph - 40;
		nckP->y2 = ph - 10;
		nckP->words = L"编号";
		dynamicNoClicks.push_back(nckP);
	}
	{	//书名
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 100;
		nckP->x2 = 350;
		nckP->y1 = ph - 40;
		nckP->y2 = ph - 10;
		nckP->words = L"书名";
		dynamicNoClicks.push_back(nckP);
	}
	{	//单价￥
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 350;
		nckP->x2 = 450;
		nckP->y1 = ph - 40;
		nckP->y2 = ph - 10;
		nckP->words = L"单价";
		dynamicNoClicks.push_back(nckP);
	}
	{	//总计￥
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 580;
		nckP->x2 = 680;
		nckP->y1 = ph - 40;
		nckP->y2 = ph - 10;
		nckP->words = L"总价";
		dynamicNoClicks.push_back(nckP);
	}
	{	//购买量
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 450;
		nckP->x2 = 580;
		nckP->y1 = ph - 40;
		nckP->y2 = ph - 10;
		nckP->words = L"数量";
		dynamicNoClicks.push_back(nckP);
	}
	for (auto bookdataP = bookstoreAdministrator.ShoppingBegin(), bookdataEndP
		= bookstoreAdministrator.ShoppingEnd(); bookdataP != bookdataEndP; bookdataP++)
	{
		if (!bookdataP->flag || bookdataP->count == 0)
			continue;
		{	//编号
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = 20;
			nckP->x2 = 80;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = std::to_wstring(k + 1);
			dynamicNoClicks.push_back(nckP);
		}
		{	//书名
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = 100;
			nckP->x2 = 350;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = bookdataP->book.name;
			dynamicNoClicks.push_back(nckP);
		}
		{	//单价￥
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = 350;
			nckP->x2 = 450;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = L"￥" + std::to_wstring(bookdataP->book.price);
			dynamicNoClicks.push_back(nckP);
		}
		{	//总计￥
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = 580;
			nckP->x2 = 680;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = L"￥" + std::to_wstring(bookdataP->count * bookdataP->book.price);
			dynamicNoClicks.push_back(nckP);
			paymentValue += bookdataP->count * bookdataP->book.price;
		}
		{	//购买量
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = 450;
			nckP->x2 = 580;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = std::to_wstring(bookdataP->count);
			dynamicNoClicks.push_back(nckP);
		}
		k++;												//下一个
	}	//for
	computeDynamicCountMax(off, k);							//计算最大滑动
	{	//支付
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 600;
		nckP->x2 = 800;
		nckP->y1 = 480 - dynamicCountMax - 20;
		nckP->y2 = 480 - dynamicCountMax - 50;
		nckP->words = L"支付";
		dynamicNoClicks.push_back(nckP);
	}
	{	//总价
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 600;
		nckP->x2 = 800;
		nckP->y1 = 480 - dynamicCountMax - 50;
		nckP->y2 = 480 - dynamicCountMax - 80;
		nckP->words = L"合计：￥" + std::to_wstring(paymentValue);
		dynamicNoClicks.push_back(nckP);
	}

}


/*********************   AdministratorBookstore   管理员查看商户   **********************/
int AdministratorBookstore::maxPage = 0;						//最大页
int AdministratorBookstore::atPage = 0;							//当前页
int AdministratorBookstore::atCount = 0;						//当前商户 未使用
int AdministratorBookstore::maxCount = 0;						//最大商户数量
std::vector<Bookstore> AdministratorBookstore::bookstores;		//商家数组
int AdministratorBookstore::FlagSort = 1;						//排序标志

AdministratorBookstore::AdministratorBookstore() {
	PageFun::AddRollbackFun(*this);								//添加返回功能

	if (maxPage == 0) {											//设置最大页
		int temp = static_cast<int>(bookstoreAdministrator.bookstores.size());
		maxPage = temp / aPage + (temp % aPage != 0);
		for (auto& it : PageAdministrator::bookstoreAdministrator.bookstores) {
			bookstores.push_back(it.second);					//拷贝一份商户信息
		}
		maxCount = static_cast<int>(bookstores.size());
	}

	constexpr int off = 40;										//行差 大于30
	constexpr int ph = 50;										//起始位置 
	constexpr int id_r = 0;										//画面位置
	constexpr int id_l = 100;
	constexpr int name_r = 100;
	constexpr int name_l = 250;
	constexpr int booksCount_r = 250;
	constexpr int booksCount_l = 350;
	constexpr int sale_r = 350;
	constexpr int sale_l = 500;
	constexpr int income_r = 500;
	constexpr int income_l = 700;
	{	//编号
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = id_r;
		ckP->x2 = id_l;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"ID";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;			//不可回退
			if (AdministratorBookstore::FlagSort & 0b1)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBookstore::bookstores.begin(),AdministratorBookstore::
				bookstores.end(), [](Bookstore& b1, Bookstore& b2)->bool {
					return b1.id < b2.id;
			});
			AdministratorBookstore::FlagSort = 0b1;
			AdministratorBookstore::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBookstore);
		};
		clicks.push_back(ckP);
	}
	{	//名字
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = name_r;
		ckP->x2 = name_l;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"店铺";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;				//不可回退
			if (AdministratorBookstore::FlagSort & 0b10)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBookstore::bookstores.begin(), AdministratorBookstore::
				bookstores.end(), [](Bookstore& b1, Bookstore& b2)->bool {
					return b1.name > b2.name;
				});
			AdministratorBookstore::FlagSort = 0b10;
			AdministratorBookstore::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBookstore);
		};
		clicks.push_back(ckP);
	}
	{	//书籍数量
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = booksCount_r;
		ckP->x2 = booksCount_l;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"图书数量";;
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;				//不可回退
			if (AdministratorBookstore::FlagSort & 0b100)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBookstore::bookstores.begin(), AdministratorBookstore::
				bookstores.end(), [](Bookstore& b1, Bookstore& b2)->bool {
					return b1.booksCount > b2.booksCount;
				});
			AdministratorBookstore::FlagSort = 0b100;
			AdministratorBookstore::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBookstore);
		};
		clicks.push_back(ckP);
	}
	{	//销售量
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = sale_r;
		ckP->x2 = sale_l;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"销售量";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;				//不可回退
			if (AdministratorBookstore::FlagSort & 0b1000)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBookstore::bookstores.begin(), AdministratorBookstore::
				bookstores.end(), [](Bookstore& b1, Bookstore& b2)->bool {
					return b1.sale > b2.sale;
				});
			AdministratorBookstore::FlagSort = 0b1000;
			AdministratorBookstore::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBookstore);
		};
		clicks.push_back(ckP);
	}
	{	//收入
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = income_r;
		ckP->x2 = income_l;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"收入";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;				//不可回退
			if (AdministratorBookstore::FlagSort & 0b10000)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBookstore::bookstores.begin(), AdministratorBookstore::
				bookstores.end(), [](Bookstore& b1, Bookstore& b2)->bool {
					return b1.income > b2.income;
				});
			AdministratorBookstore::FlagSort = 0b10000;
			AdministratorBookstore::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBookstore);
		};
		clicks.push_back(ckP);
	}

	int index = atPage * aPage;									//读取位置
	for (int k = 0; k < aPage && index < maxCount; index++ ,k++) {
		{	//编号
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = id_r;
			nckP->x2 = id_l;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = std::to_wstring(bookstores[index].id);
			noClicks.push_back(nckP);
		}
		{	//名字
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = name_r;
			nckP->x2 = name_l;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = bookstores[index].name;
			noClicks.push_back(nckP);
		}
		{	//书籍数量
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = booksCount_r;
			nckP->x2 = booksCount_l;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = std::to_wstring(bookstores[index].booksCount);
			noClicks.push_back(nckP);
		}
		{	//销售量
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = sale_r;
			nckP->x2 = sale_l;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = std::to_wstring(bookstores[index].sale);
			noClicks.push_back(nckP);
		}
		{	//收入
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = income_r;
			nckP->x2 = income_l;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = L"￥" + std::to_wstring(bookstores[index].income);
			noClicks.push_back(nckP);
		}
	}//for
	{	//上一页
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 700;
		ckP->x2 = 800;
		ckP->y1 = 200;
		ckP->y2 = 230;
		ckP->words = L"上一页";
		ckP->fun = []()->std::shared_ptr<Page> {
			if (AdministratorBookstore::atPage > 0) {
				PageAdministrator::FlagNORollback = true;		//不可回退
				AdministratorBookstore::atPage--;
				return std::shared_ptr<Page>(new AdministratorBookstore);
			}
			return nullptr;
		};
		clicks.push_back(ckP);
	}
	{	//下一页
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 700;
		ckP->x2 = 800;
		ckP->y1 = 250;
		ckP->y2 = 280;
		ckP->words = L"下一页";
		ckP->fun = [index]()->std::shared_ptr<Page> {
			if (AdministratorBookstore::atPage  + 1 < AdministratorBookstore::maxPage) {
				PageAdministrator::FlagNORollback = true;		//不可回退
				AdministratorBookstore::atPage++;
				return std::shared_ptr<Page>(new AdministratorBookstore);
			}
			return nullptr;
		};
		clicks.push_back(ckP);
	}
	{	//当前页
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 700;
		nckP->x2 = 800;
		nckP->y1 = 440;
		nckP->y2 = 470;
		nckP->words = std::to_wstring(atPage + 1) + L"/" + std::to_wstring(maxPage);
		noClicks.push_back(nckP);
	}

}

/*********************   AdministratorBook   管理员查看图书   **********************/
int AdministratorBook::maxPage = 0;								//最大页
int AdministratorBook::atPage = 0;								//当前页
int AdministratorBook::atCount = 0;								//当前商户 未使用
int AdministratorBook::maxCount = 0;							//最大商户数量
std::vector<Book> AdministratorBook::books;						//books数组
int AdministratorBook::FlagSort = 1;							//排序标志

AdministratorBook::AdministratorBook() {
	PageFun::AddRollbackFun(*this);								//添加返回功能

	if (maxPage == 0) {											//设置最大页
		int temp = static_cast<int>(bookstoreAdministrator.books.size());
		maxPage = temp / aPage + (temp % aPage != 0);
		books = PageAdministrator::bookstoreAdministrator.books;//拷贝一份商户信息
		maxCount = static_cast<int>(books.size());
	}

	constexpr int off = 40;										//行差 大于30
	constexpr int ph = 50;										//起始
	constexpr int bookstoreId_l = 0;							//画面范围
	constexpr int bookstoreId_r = 100;
	constexpr int name_l = 100;
	constexpr int name_r = 325;
	constexpr int repertory_l = 325;
	constexpr int repertory_r = 415;
	constexpr int sale_l = 420;
	constexpr int sale_r = 510;
	constexpr int price_l = 510;
	constexpr int price_r = 600;
	constexpr int income_l = 600;
	constexpr int income_r = 700;
	{	//商家编号
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = bookstoreId_l;
		ckP->x2 = bookstoreId_r;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"商家";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;		//不可回退
			if (AdministratorBook::FlagSort & 0b1)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBook::books.begin(), AdministratorBook::
				books.end(), [](Book& b1, Book& b2)->bool {
					return b1.bookstoreId < b2.bookstoreId;
				});
			AdministratorBook::FlagSort = 0b1;
			AdministratorBook::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBook);
		};
		clicks.push_back(ckP);
	}
	{	//名字
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = name_l;
		ckP->x2 = name_r;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"书名";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;		//不可回退
			if (AdministratorBook::FlagSort & 0b10)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBook::books.begin(), AdministratorBook::
				books.end(), [](Book& b1, Book& b2)->bool {
					return b1.name > b2.name;
				});
			AdministratorBook::FlagSort = 0b10;
			AdministratorBook::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBook);
		};
		clicks.push_back(ckP);
	}
	{	//库存
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = repertory_l;
		ckP->x2 = repertory_r;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"库存";;
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;			//不可回退
			if (AdministratorBook::FlagSort & 0b100)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBook::books.begin(), AdministratorBook::
				books.end(), [](Book& b1, Book& b2)->bool {
					return b1.repertory > b2.repertory;
				});
			AdministratorBook::FlagSort = 0b100;
			AdministratorBook::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBook);
		};
		clicks.push_back(ckP);
	}
	{	//销量
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = sale_l;
		ckP->x2 = sale_r;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"销量";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;			//不可回退
			if (AdministratorBook::FlagSort & 0b1000)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBook::books.begin(), AdministratorBook::
				books.end(), [](Book& b1, Book& b2)->bool {
					return b1.sale > b2.sale;
				});
			AdministratorBook::FlagSort = 0b1000;
			AdministratorBook::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBook);
		};
		clicks.push_back(ckP);
	}
	{	//价格
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = price_l;
		ckP->x2 = price_r;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"价格";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;			//不可回退
			if (AdministratorBook::FlagSort & 0b10000)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBook::books.begin(), AdministratorBook::
				books.end(), [](Book& b1, Book& b2)->bool {
					return b1.price > b2.price;
				});
			AdministratorBook::FlagSort = 0b10000;
			AdministratorBook::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBook);
		};
		clicks.push_back(ckP);
	}
	{	//收入
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = income_l;
		ckP->x2 = income_r;
		ckP->y1 = ph - 40;
		ckP->y2 = ph - 10;
		ckP->words = L"收入";
		ckP->fun = []()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;			//不可回退
			if (AdministratorBook::FlagSort & 0b100000)			//已经排序过了
				return nullptr;
			std::sort(AdministratorBook::books.begin(), AdministratorBook::
				books.end(), [](Book& b1, Book& b2)->bool {
					return b1.price * b1.sale > b2.price * b2.sale;
				});
			AdministratorBook::FlagSort = 0b100000;
			AdministratorBook::atPage = 0;
			return std::shared_ptr<Page>(new AdministratorBook);
		};
		clicks.push_back(ckP);
	}

	int index = atPage * aPage;									//读取位置
	for (int k = 0; k < aPage && index < maxCount; index++, k++) {
		{	//商户id
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = bookstoreId_l;
			nckP->x2 = bookstoreId_r;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = bookstoreAdministrator.bookstores[books[index].bookstoreId].name;
			noClicks.push_back(nckP);
		}
		{	//名字
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = name_l;
			nckP->x2 = name_r;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = books[index].name;
			noClicks.push_back(nckP);
		}
		{	//库存
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = repertory_l;
			nckP->x2 = repertory_r;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = std::to_wstring(books[index].repertory);
			noClicks.push_back(nckP);
		}
		{	//销量
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = sale_l;
			nckP->x2 = sale_r;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = std::to_wstring(books[index].sale);
			noClicks.push_back(nckP);
		}
		{	//价格
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = price_l;
			nckP->x2 = price_r;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = L"￥" + std::to_wstring(books[index].price);
			noClicks.push_back(nckP);
		}
		{	//价格
			std::shared_ptr<NoClick> nckP(new NoClick);
			nckP->x1 = income_l;
			nckP->x2 = income_r;
			nckP->y1 = ph + k * off;
			nckP->y2 = ph + 30 + k * off;
			nckP->words = std::to_wstring(books[index].price * books[index].sale);
			noClicks.push_back(nckP);
		}
	}//for
	{	//上一页
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 700;
		ckP->x2 = 800;
		ckP->y1 = 200;
		ckP->y2 = 230;
		ckP->words = L"上一页";
		ckP->fun = []()->std::shared_ptr<Page> {
			if (AdministratorBook::atPage > 0) {
				PageAdministrator::FlagNORollback = true;		//不可回退
				AdministratorBook::atPage--;
				return std::shared_ptr<Page>(new AdministratorBook);
			}
			return nullptr;
		};
		clicks.push_back(ckP);
	}
	{	//下一页
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 700;
		ckP->x2 = 800;
		ckP->y1 = 250;
		ckP->y2 = 280;
		ckP->words = L"下一页";
		ckP->fun = [index]()->std::shared_ptr<Page> {
			if (AdministratorBook::atPage + 1 < AdministratorBook::maxPage) {
				PageAdministrator::FlagNORollback = true;		//不可回退
				AdministratorBook::atPage++;
				return std::shared_ptr<Page>(new AdministratorBook);
			}
			return nullptr;
		};
		clicks.push_back(ckP);
	}
	{	//当前页
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 700;
		nckP->x2 = 800;
		nckP->y1 = 440;
		nckP->y2 = 470;
		nckP->words = std::to_wstring(atPage + 1) + L"/" + std::to_wstring(maxPage);
		noClicks.push_back(nckP);
	}
}


/*********************   AdministratorAddBookstore   管理员增加商户   ********************/
std::wstring AdministratorAddBookstore::position;
std::wstring AdministratorAddBookstore::portrait;
AdministratorWriteBookstore AdministratorAddBookstore::data;

AdministratorAddBookstore::AdministratorAddBookstore() :Page() {
	PageFun::AddRollbackFun(*this);								//添加返回功能
	dynamicFlag = true;											//按动态页绘制画面
	if (data.id < 1) {data.id = static_cast<int>(bookstoreAdministrator.bookstores.size()) + 1;}
	
	
	ImportData* nameP = nullptr;
	ImportData* portraitP = nullptr;
	ImportData* bookPositionP = nullptr;

	constexpr int w1 = 100;

	{	//商家头像
		std::shared_ptr<NoClickPortrait> nckP(new NoClickPortrait);
		nckP->x1 = 2;
		nckP->x2 = 92;
		nckP->y1 = 2;
		nckP->y2 = 92;
		nckP->high = 24;
		nckP->position = data.portrait;
		noClicks.push_back(nckP);
	}
	{	//id
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w1 + 100;
		nckP->y1 = 10;
		nckP->y2 = 40;
		nckP->high = 24;
		nckP->words = L"ID：";
		noClicks.push_back(nckP);
	}
	{	//id号
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1 + 100;
		nckP->x2 = w1 + 200;
		nckP->y1 = 10;
		nckP->y2 = 40;
		nckP->high = 24;
		nckP->words = std::to_wstring(data.id);
		noClicks.push_back(nckP);
	}

	{	//店名
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w1 + 100;
		nckP->y1 = 50;
		nckP->y2 = 80;
		nckP->high = 24;
		nckP->words = L"店名：";
		noClicks.push_back(nckP);
	}
	{	//输入框
		std::shared_ptr<ClickImportr> ckP(new ClickImportr(data.name));
		ckP->x1 = w1 + 100;
		ckP->x2 = w1 + 500;
		ckP->y1 = 50;
		ckP->y2 = 80;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		nameP = &ckP->importData;
		ckP->fun = ([nameP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(nameP);		//绑定输入
			return nullptr; });
		clicks.push_back(ckP);
	}
	{	//头像
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w1 + 100;
		nckP->y1 = 90;
		nckP->y2 = 120;
		nckP->high = 24;
		nckP->words = L"头像：";
		noClicks.push_back(nckP);
	}
	{	//输入框
		std::shared_ptr<ClickImportr> ckP(new ClickImportr(portrait));
		ckP->x1 = w1 + 100;
		ckP->x2 = w1 + 500;
		ckP->y1 = 90;
		ckP->y2 = 120;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		portraitP = &ckP->importData;
		ckP->fun = ([portraitP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(portraitP);		//绑定输入
			return nullptr; });
		clicks.push_back(ckP);
	}
	{	//图片预览
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = w1 + 520;
		ckP->x2 = w1 + 620;
		ckP->y1 = 90;
		ckP->y2 = 120;
		ckP->high = 24;
		ckP->words = L"头像预览";
		ckP->fun = ([this]()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;		//不可回退
			this->data.portrait = L"books\\BookstorePortrait\\" + portrait;
			return std::shared_ptr<Page>(new AdministratorAddBookstore); });
		clicks.push_back(ckP);
	}

	{	//文件
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w1 + 100;
		nckP->y1 = 130;
		nckP->y2 = 160;
		nckP->high = 24;
		nckP->words = L"文件：";
		noClicks.push_back(nckP);
	}
	{	//输入框
		std::shared_ptr<ClickImportr> ckP(new ClickImportr(position));
		ckP->x1 = w1 + 100;
		ckP->x2 = w1 + 500;
		ckP->y1 = 130;
		ckP->y2 = 160;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		bookPositionP = &ckP->importData;
		ckP->fun = ([bookPositionP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(bookPositionP);		//绑定输入
			return nullptr; });
		clicks.push_back(ckP);
	}
}


/*********************     AdministratorAddBook    管理员增加书     ********************/
std::wstring AdministratorAddBook::postion;
std::wstring AdministratorAddBook::bookPostion;
std::wstring AdministratorAddBook::name;
std::wstring AdministratorAddBook::portrait;
int AdministratorAddBook::indexId = 0;
bool AdministratorAddBook::FlagAdd = false;
bool AdministratorAddBook::FlagPhoto = false;
Book AdministratorAddBook::bookData;

AdministratorAddBook::AdministratorAddBook() :Page() {
	PageFun::AddRollbackFun(*this);								//添加返回功能
	dynamicFlag = true;											//按动态页绘制画面

	if (bookData.price < 0) {bookData.price = 0;}
	
	ImportDataBase* nameP = nullptr;
	ImportData* positionP = nullptr;
	ImportDataBase* repertoryP = nullptr;
	ImportDataBase* priceP = nullptr;
	ImportDataBase* indexIDP = nullptr;

	constexpr int w1 = 100;
	constexpr int w2 = 0; 

	{	//商家头像
		std::shared_ptr<NoClickPortrait> nckP(new NoClickPortrait);
		nckP->x1 = 2;
		nckP->x2 = 92;
		nckP->y1 = 2;
		nckP->y2 = 92;
		nckP->high = 24;
		nckP->position = portrait;
		noClicks.push_back(nckP);
	}
	{	//ID
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w1 + 100;
		nckP->y1 = 10;
		nckP->y2 = 40;
		nckP->high = 24;
		nckP->words = L"ID：";
		noClicks.push_back(nckP);
	}
	{	//ID 输入框
		std::shared_ptr<ClickImportrNumber> ckP(new ClickImportrNumber(indexId));
		ckP->x1 = w1 + 100;
		ckP->x2 = w1 + 200;
		ckP->y1 = 10;
		ckP->y2 = 40;
		ckP->high = 24;
		ckP->importData.maxCount = 4;
		indexIDP = &ckP->importData;
		ckP->fun = ([indexIDP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(indexIDP);		//绑定输入
			return nullptr; });
		clicks.push_back(ckP);
	}
	{	//搜索商家
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = w1 + 220;
		ckP->x2 = w1 + 320;
		ckP->y1 = 10;
		ckP->y2 = 40;
		ckP->high = 24;
		ckP->words = L"搜索商家";
		AdministratorAddBook* tempP = this;
		ckP->fun = ([tempP]()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;		//不可回退

			std::wifstream file("books\\Bookstores.txt");
			while (!file.eof()) {
				AdministratorWriteBookstore bs;
				bs.read(file);
				if (bs.id == tempP->indexId) {
					tempP->name = bs.name;
					tempP->postion = bs.bookPosition;
					tempP->portrait = bs.portrait;
					FlagAdd = true;
					return std::shared_ptr<Page>(new AdministratorAddBook);
				}
			}
			tempP->name = L"未找到商家";
			tempP->postion = L"地址加载失败";
			FlagAdd = false;

			return std::shared_ptr<Page>(new AdministratorAddBook); });
		clicks.push_back(ckP);
	}
	{	//商家名字
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w1 + 100;
		nckP->y1 = 50;
		nckP->y2 = 80;
		nckP->high = 24;
		nckP->words = L"商家：";
		noClicks.push_back(nckP);
	}
	{	//商家名字
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 + 100;
		nckP->x2 = w1 + 700;
		nckP->y1 = 50;
		nckP->y2 = 80;
		nckP->high = 24;
		nckP->words = name;
		noClicks.push_back(nckP);
	}
	{	//路径
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w1 + 100;
		nckP->y1 = 90;
		nckP->y2 = 120;
		nckP->high = 24;
		nckP->words = L"路径：";
		noClicks.push_back(nckP);
	}
	{	//路径
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 + 100;
		nckP->x2 = w1 + 700;
		nckP->y1 = 90;
		nckP->y2 = 120;
		nckP->high = 24;
		nckP->words = postion;
		noClicks.push_back(nckP);
	}
	{	//书名
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w2;
		nckP->x2 = w2 + 100;
		nckP->y1 = 130;
		nckP->y2 = 160;
		nckP->high = 24;
		nckP->words = L"书名：";
		noClicks.push_back(nckP);
	}
	{	//书名 输入框
		std::shared_ptr<ClickImportr> ckP(new ClickImportr(bookData.name));
		ckP->x1 = w2 + 100;
		ckP->x2 = w2 + 300;
		ckP->y1 = 130;
		ckP->y2 = 160;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		nameP = &ckP->importData;
		ckP->fun = ([nameP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(nameP);		//绑定输入
			return nullptr; });
		clicks.push_back(ckP);
	}
	{	//图片
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w2;
		nckP->x2 = w2 + 100;
		nckP->y1 = 170;
		nckP->y2 = 200;
		nckP->high = 24;
		nckP->words = L"图片：";
		noClicks.push_back(nckP);
	}
	{	//图片 输入框
		std::shared_ptr<ClickImportr> ckP(new ClickImportr(bookPostion));
		ckP->x1 = w2 + 100;
		ckP->x2 = w2 + 420;
		ckP->y1 = 170;
		ckP->y2 = 200;
		ckP->high = 24;
		ckP->importData.maxCount = 24;
		positionP = &ckP->importData;
		ckP->fun = ([positionP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(positionP);		//绑定输入
			return nullptr; });
		clicks.push_back(ckP);
	}
	{	//图片
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w2 + 430;
		nckP->x2 = w2 + 580;
		nckP->y1 = 130;
		nckP->y2 = 360;
		nckP->high = 24;
		nckP->position = bookData.photo_200x150;
		noClicks.push_back(nckP);
	}

	{	//图片
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w2 + 590;
		nckP->x2 = w2 + 695;
		nckP->y1 = 130;
		nckP->y2 = 270;
		nckP->high = 24;
		nckP->position = bookData.photo_140x105;
		noClicks.push_back(nckP);
	}
	{	//图片
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w2 + 700;
		nckP->x2 = w2 + 775;
		nckP->y1 = 130;
		nckP->y2 = 205;
		nckP->high = 24;
		nckP->position = bookData.photo_100x75;
		noClicks.push_back(nckP);
	}
	{	//价格
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w2;
		nckP->x2 = w2 + 100;
		nckP->y1 = 210;
		nckP->y2 = 240;
		nckP->high = 24;
		nckP->words = L"价格：";
		noClicks.push_back(nckP);
	}
	{	//价格 输入框
		std::shared_ptr<ClickImportrNumber> ckP(new ClickImportrNumber(bookData.price));
		ckP->x1 = w2 + 100;
		ckP->x2 = w2 + 300;
		ckP->y1 = 210;
		ckP->y2 = 240;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		priceP = &ckP->importData;
		ckP->fun = ([priceP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(priceP);		//绑定输入
			return nullptr; });
		clicks.push_back(ckP);
	}
	{	//库存
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w2;
		nckP->x2 = w2 + 100;
		nckP->y1 = 250;
		nckP->y2 = 280;
		nckP->high = 24;
		nckP->words = L"库存：";
		noClicks.push_back(nckP);
	}
	{	//库存 输入框
		std::shared_ptr<ClickImportrNumber> ckP(new ClickImportrNumber(bookData.repertory));
		ckP->x1 = w2 + 100;
		ckP->x2 = w2 + 300;
		ckP->y1 = 250;
		ckP->y2 = 280;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		repertoryP = &ckP->importData;
		ckP->fun = ([repertoryP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(repertoryP);		//绑定输入
			return nullptr; });
		clicks.push_back(ckP);
	}

	{	//图片预览
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = w2 + 320;
		ckP->x2 = w2 + 420;
		ckP->y1 = 130;
		ckP->y2 = 160;
		ckP->high = 24;
		ckP->words = L"图片预览";
		ckP->fun = ([this]()->std::shared_ptr<Page> {
			PageAdministrator::FlagNORollback = true;		//不可回退
			this->findBookPhoto();
			return std::shared_ptr<Page>(new AdministratorAddBook); });
		clicks.push_back(ckP);
	}

	{	//添加
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = w1;
		ckP->x2 = w1 + 80;
		ckP->y1 = 300;
		ckP->y2 = 360;
		ckP->high = 24;
		ckP->words = L"添加";
		AdministratorAddBook* tempP = this;
		ckP->fun = [this]()->std::shared_ptr<Page> {
			this->write();
			this->flush();
			PageAdministrator::FlagNORollback = true;		//不可回退
			return std::shared_ptr<Page>(new AdministratorAddBook);
			};
		clicks.push_back(ckP);
	}

}


/***************************     LoginPagek    登入页     ************************/
LoginPage::LoginPage() :Page() {
	PageFun::AccessLogoPage(*this);								//Logo
	PageFun::AddExitkFun(*this);								//退出
	PageFun::AddVersionFun1_4(*this);							//版本
	dynamicFlag = true;											//按动态页绘制画面

	ImportDataBase* nameP;
	ImportDataBase* keyP;

	constexpr int w1 = 150;
	constexpr int w2 = 250;
	constexpr int w3 = 550;

	
	{	//用户
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 90;
		nckP->y2 = 120;
		nckP->high = 24;
		nckP->words = L"用户：";
		noClicks.push_back(nckP);
	}
	{	//用户名
		std::shared_ptr<ClickImportr> ckP(new ClickImportr(name));
		ckP->x1 = w2;
		ckP->x2 = w3;
		ckP->y1 = 90;
		ckP->y2 = 120;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		nameP = &ckP->importData;
		ckP->fun = [nameP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(nameP);		//绑定输入
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//密码
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 130;
		nckP->y2 = 160;
		nckP->high = 24;
		nckP->words = L"密码：";
		noClicks.push_back(nckP);
	}
	{	//密码
		std::shared_ptr<ClickImportrPassword> ckP(new ClickImportrPassword(key));
		ckP->x1 = w2;
		ckP->x2 = w3;
		ckP->y1 = 130;
		ckP->y2 = 160;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		keyP = &ckP->importData;
		ckP->fun = [keyP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(keyP);		//绑定输入
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//状态
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 170;
		nckP->y2 = 200;
		nckP->high = 24;
		nckP->words = L"状态：";
		noClicks.push_back(nckP);
	}
	{	//注册状态 && 登入
		std::shared_ptr<NoClickDynamicWordsL> nckP(new NoClickDynamicWordsL);
		nckP->x1 = w2;
		nckP->x2 = w3;
		nckP->y1 = 170;
		nckP->y2 = 200;
		nckP->high = 24;
		nckP->wordsArr.push_back(L"");					//0
		nckP->wordsArr.push_back(L"登入成功");			//1
		nckP->wordsArr.push_back(L"用户名不存在");		//2
		nckP->wordsArr.push_back(L"密码错误");			//3
		noClicks.push_back(nckP);
		int& counttemp = nckP->count;


		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = w1;
		ckP->x2 = w2;
		ckP->y1 = 210;
		ckP->y2 = 240;
		ckP->high = 24;
		ckP->words = L"登入淘书";
		ckP->fun = [this, &counttemp]()->std::shared_ptr<Page> {
			counttemp = PageAdministrator::consumerAdministrator.enter(name, key);
			if (counttemp == 1) {return std::shared_ptr<Page>(new MainPage1_2);}
			else {return nullptr;} 
		};
		clicks.push_back(ckP);
	}
	{	//登入
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = w2 + 45;
		ckP->x2 = w2 + 145;
		ckP->y1 = 210;
		ckP->y2 = 240;
		ckP->high = 24;
		ckP->words = L"注册账号";
		ckP->fun = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new RegisterPage); };
		clicks.push_back(ckP);
	}
	{	//稍后登入
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = w2 + 190;
		ckP->x2 = w2 + 290;
		ckP->y1 = 210;
		ckP->y2 = 240;
		ckP->high = 24;
		ckP->words = L"稍后登入";
		ckP->fun = []()->std::shared_ptr<Page> {
			return std::shared_ptr<Page>(new MainPage1_2); };
		clicks.push_back(ckP);
	}
	{	//常见问题
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 50;
		nckP->y1 = 270;
		nckP->y2 = 300;
		nckP->high = 24;
		nckP->words = L"常见问题：";
		noClicks.push_back(nckP);
	}
	{	//规则1
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 200;
		nckP->y1 = 310;
		nckP->y2 = 340;
		nckP->high = 24;
		nckP->words = L"1.如果您没有账号可以注册一个，请点击“注册账号”。";
		noClicks.push_back(nckP);
	}
	{	//规则2
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 200;
		nckP->y1 = 350;
		nckP->y2 = 380;
		nckP->high = 24;
		nckP->words = L"2.忘记密码请联系管理员。";
		noClicks.push_back(nckP);
	}
	{	//规则3
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 200;
		nckP->y1 = 390;
		nckP->y2 = 420;
		nckP->high = 24;
		nckP->words = L"3.点击“稍后登入”就能进入，但是不能购买图书。";
		noClicks.push_back(nckP);
	}

}


/***************************     RegisterPage    注册页     ************************/
RegisterPage::RegisterPage() :Page() {
	PageFun::AccessLogoPage(*this);								//Logo
	PageFun::AddExitkFun(*this);								//退出
	PageFun::AddRollbackFun(*this);								//返回
	dynamicFlag = true;											//按动态页绘制画面

	ImportDataBase* nameP;
	ImportDataBase* keyP;
	ImportDataBase* key2P;
	ImportDataNumber* headP;

	constexpr int w1 = 150;
	constexpr int w2 = 250;
	constexpr int w3 = 550;


	{	//账  号  注  册
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1 - 100;
		nckP->x2 = w3 + 200;
		nckP->y1 = 20;
		nckP->y2 = 70;
		nckP->high = 48;
		nckP->words = L"账  号  注  册";
		noClicks.push_back(nckP);
	}

	{	//用户
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 90;
		nckP->y2 = 120;
		nckP->high = 24;
		nckP->words = L"用户：";
		noClicks.push_back(nckP);
	}
	{	//用户名
		std::shared_ptr<ClickImportr> ckP(new ClickImportr(name));
		ckP->x1 = w2;
		ckP->x2 = w3;
		ckP->y1 = 90;
		ckP->y2 = 120;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		nameP = &ckP->importData;
		ckP->fun = [nameP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(nameP);		//绑定输入
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//密码
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 130;
		nckP->y2 = 160;
		nckP->high = 24;
		nckP->words = L"密码：";
		noClicks.push_back(nckP);
	}
	{	//密码
		std::shared_ptr<ClickImportrPassword> ckP(new ClickImportrPassword(key));
		ckP->x1 = w2;
		ckP->x2 = w3;
		ckP->y1 = 130;
		ckP->y2 = 160;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		keyP = &ckP->importData;
		ckP->fun = [keyP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(keyP);		//绑定输入
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//密码2
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 170;
		nckP->y2 = 200;
		nckP->high = 24;
		nckP->words = L"确认：";
		noClicks.push_back(nckP);
	}
	{	//密码2
		std::shared_ptr<ClickImportrPassword> ckP(new ClickImportrPassword(key2));
		ckP->x1 = w2;
		ckP->x2 = w3;
		ckP->y1 = 170;
		ckP->y2 = 200;
		ckP->high = 24;
		ckP->importData.maxCount = 18;
		key2P = &ckP->importData;
		ckP->fun = [key2P]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(key2P);		//绑定输入
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//状态
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 210;
		nckP->y2 = 240;
		nckP->high = 24;
		nckP->words = L"状态：";
		noClicks.push_back(nckP);
	}
	{	//注册状态 && 注册
		std::shared_ptr<NoClickDynamicWordsL> nckP(new NoClickDynamicWordsL);
		nckP->x1 = w2;
		nckP->x2 = w3;
		nckP->y1 = 210;
		nckP->y2 = 240;
		nckP->high = 24;
		nckP->wordsArr.push_back(L"");					//0
		nckP->wordsArr.push_back(L"注册成功");			//1
		nckP->wordsArr.push_back(L"两次密码不同");		//2
		nckP->wordsArr.push_back(L"用户名已经存在");	//3
		nckP->wordsArr.push_back(L"密码为空");			//4
		nckP->wordsArr.push_back(L"密码包含空格");		//5
		nckP->wordsArr.push_back(L"用户名为空");		//6
		noClicks.push_back(nckP);
		int& counttemp = nckP->count;

		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = w3;
		ckP->x2 = w3 + 100;
		ckP->y1 = 170;
		ckP->y2 = 200;
		ckP->high = 24;
		ckP->words = L"注册";
		ckP->fun = [this,&counttemp]()->std::shared_ptr<Page> {
			counttemp = PageAdministrator::consumerAdministrator.registerConsumer(
				key, key2, name, L"client\\portrait\\" + std::to_wstring(head) + L".jpeg");
			return nullptr; };
		clicks.push_back(ckP);



	}
	{	//规则
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 50;
		nckP->y1 = 250;
		nckP->y2 = 280;
		nckP->high = 24;
		nckP->words = L"注册规则：";
		noClicks.push_back(nckP);
	}
	{	//规则1
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 50;
		nckP->y1 = 290;
		nckP->y2 = 320;
		nckP->high = 24;
		nckP->words = L"1.用户名，不支持同名，且最长支持12位。";
		noClicks.push_back(nckP);
	}
	{	//规则2
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 200;
		nckP->y1 = 325;
		nckP->y2 = 355;
		nckP->high = 24;
		nckP->words = L"2.密码可以是字母.数字.汉字，但不能包含不可显示字符。";
		noClicks.push_back(nckP);
	}
	{	//规则3
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 200;
		nckP->y1 = 360;
		nckP->y2 = 390;
		nckP->high = 24;
		nckP->words = L"3.密码长度为1 - 12位。";
		noClicks.push_back(nckP);
	}
	{	//规则4
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 200;
		nckP->y1 = 395;
		nckP->y2 = 425;
		nckP->high = 24;
		nckP->words = L"4.头像可以输入数字0 - 9，选中一个您喜欢的头像";
		noClicks.push_back(nckP);
	}
	{	//注
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1 - 50;
		nckP->x2 = w3 + 200;
		nckP->y1 = 430;
		nckP->y2 = 460;
		nckP->high = 24;
		nckP->words = L"注：中文密码存在安全隐患，请不用模仿本程序。";
		noClicks.push_back(nckP);
	}
	{	//头像
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = w1 - 150;
		nckP->x2 = w1 - 70;
		nckP->y1 = 210;
		nckP->y2 = 240;
		nckP->high = 24;
		nckP->words = L"头像：";
		noClicks.push_back(nckP);
	}
	{	//头像ID 输入框
		std::shared_ptr<ClickImportrNumber> ckP(new ClickImportrNumber(head));
		ckP->x1 = w1 - 70;
		ckP->x2 = w1 - 20;
		ckP->y1 = 210;
		ckP->y2 = 240;
		ckP->high = 24;
		ckP->importData.maxCount = 1;
		headP = &ckP->importData;
		ckP->fun = ([headP]()->std::shared_ptr<Page> {
			PageAdministrator::SetCharImport(headP);		//绑定输入
			return nullptr; });
		clicks.push_back(ckP);
	}
	{	//商家头像 && 查看头像
		std::shared_ptr<NoClickPortrait> nckP(new NoClickPortrait);
		nckP->x1 = 25;
		nckP->x2 = 115;
		nckP->y1 = 92;
		nckP->y2 = 182;
		nckP->high = 24;
		nckP->position = L"client\\portrait\\0.jpeg";
		noClicks.push_back(nckP);

		std::wstring& wstr = nckP->position;
		int& headtemp = head;

		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = w1 - 130;
		ckP->x2 = w1 - 20;
		ckP->y1 = 250;
		ckP->y2 = 280;
		ckP->high = 24;
		ckP->words = L"查看头像";
		ckP->fun = ([&wstr, &headtemp]()->std::shared_ptr<Page> {
			wstr = L"client\\portrait\\" + std::to_wstring(headtemp) + L".jpeg";
			return nullptr; });
		clicks.push_back(ckP);
	}

}


/***************************     LoadingPage    加载页     ************************/
LoadingPage::LoadingPage() {
	dynamicFlag = true;											//按动态页绘制画面
}


/***************************     WalletPage    钱包页     ************************/
WalletPage::WalletPage() :Page() {
	PageFun::AddRollbackFun(*this);								//返回


	constexpr int k1 = 60;
	constexpr int k2 = 100;
	constexpr int w1 = 110;
	constexpr int w2 = 210;
	constexpr int w3 = 310;
	constexpr int w4 = 530;


	{	//头像
		std::shared_ptr<NoClickPortrait> nckP(new NoClickPortrait);
		nckP->x1 = 12;
		nckP->x2 = 102;
		nckP->y1 = 12;
		nckP->y2 = 102;
		nckP->high = 24;
		nckP->position = consumer.portrait;
		noClicks.push_back(nckP);
	}
	{	//名字
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 110;
		nckP->x2 = 350;
		nckP->y1 = 20;
		nckP->y2 = 50;
		nckP->high = 24;
		nckP->words = consumer.name;
		noClicks.push_back(nckP);
	}
	{	//余额
		std::shared_ptr<NoClickDynamicValue> nckP(new NoClickDynamicValue);
		nckP->x1 = 110;
		nckP->x2 = 350;
		nckP->y1 = 60;
		nckP->y2 = 90;
		nckP->high = 24;
		int& balancetemp = consumer.balance;
		nckP->dynamicWordsFun = [&balancetemp]() ->std::wstring {
			return L"余额：" + std::to_wstring(balancetemp);
		};
		noClicks.push_back(nckP);
	}

	{	//充值窗口
		std::shared_ptr<NoClickPortrait> nckP(new NoClickPortrait);
		nckP->x1 = 20;
		nckP->x2 = 550;
		nckP->y1 = 120;
		nckP->y2 = 460;
		nckP->high = 24;
		noClicks.push_back(nckP);
	}
	{	//6
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 150;
		nckP->y2 = 180;
		nckP->high = 24;
		nckP->words = L"￥6";
		noClicks.push_back(nckP);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = k1;
		ckP->x2 = k2;
		ckP->y1 = 142;
		ckP->y2 = 182;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_vaule(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_vaule(p, -6);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//18
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 190;
		nckP->y2 = 220;
		nckP->high = 24;
		nckP->words = L"￥18";
		noClicks.push_back(nckP);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = k1;
		ckP->x2 = k2;
		ckP->y1 = 182;
		ckP->y2 = 222;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_vaule(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr;};
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this,p]()->std::shared_ptr<Page> {
			this->set_vaule(p, -18);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr;};
		clicks.push_back(ckP);
	}
	{	//30
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 230;
		nckP->y2 = 260;
		nckP->high = 24;
		nckP->words = L"￥30";
		noClicks.push_back(nckP);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = k1;
		ckP->x2 = k2;
		ckP->y1 = 222;
		ckP->y2 = 262;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_vaule(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_vaule(p, -30);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//68
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 270;
		nckP->y2 = 300;
		nckP->high = 24;
		nckP->words = L"￥68";
		noClicks.push_back(nckP);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = k1;
		ckP->x2 = k2;
		ckP->y1 = 262;
		ckP->y2 = 302;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_vaule(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_vaule(p, -68);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//98
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 310;
		nckP->y2 = 340;
		nckP->high = 24;
		nckP->words = L"￥98";
		noClicks.push_back(nckP);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = k1;
		ckP->x2 = k2;
		ckP->y1 = 302;
		ckP->y2 = 342;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_vaule(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_vaule(p, -98);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//198
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 350;
		nckP->y2 = 380;
		nckP->high = 24;
		nckP->words = L"￥198";
		noClicks.push_back(nckP);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = k1;
		ckP->x2 = k2;
		ckP->y1 = 342;
		ckP->y2 = 382;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_vaule(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_vaule(p, -198);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//328
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w1;
		nckP->x2 = w2;
		nckP->y1 = 390;
		nckP->y2 = 420;
		nckP->high = 24;
		nckP->words = L"￥328";
		noClicks.push_back(nckP);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = k1;
		ckP->x2 = k2;
		ckP->y1 = 382;
		ckP->y2 = 422;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_vaule(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_vaule(p, -328);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//X1
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w3;
		nckP->x2 = w4;
		nckP->y1 = 150;
		nckP->y2 = 180;
		nckP->high = 24;
		nckP->words = L"￥648 * 1";
		noClicks.push_back(nckP);
	}
	{	//X5
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w3;
		nckP->x2 = w4;
		nckP->y1 = 190;
		nckP->y2 = 220;
		nckP->high = 24;
		nckP->words = L"￥648 * 5";
		noClicks.push_back(nckP);
	}
	{	//X10
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w3;
		nckP->x2 = w4;
		nckP->y1 = 230;
		nckP->y2 = 260;
		nckP->high = 24;
		nckP->words = L"￥648 * 10";
		noClicks.push_back(nckP);
	}
	{	//X50
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w3;
		nckP->x2 = w4;
		nckP->y1 = 270;
		nckP->y2 = 300;
		nckP->high = 24;
		nckP->words = L"￥648 * 50";
		noClicks.push_back(nckP);
	}
	{	//X100
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w3;
		nckP->x2 = w4;
		nckP->y1 = 310;
		nckP->y2 = 340;
		nckP->high = 24;
		nckP->words = L"￥648 * 100";
		noClicks.push_back(nckP);
	}
	{	//X500
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w3;
		nckP->x2 = w4;
		nckP->y1 = 350;
		nckP->y2 = 380;
		nckP->high = 24;
		nckP->words = L"￥648 * 500";
		noClicks.push_back(nckP);
	}
	{	//X1000
		std::shared_ptr<NoClickL> nckP(new NoClickL);
		nckP->x1 = w3;
		nckP->x2 = w4;
		nckP->y1 = 390;
		nckP->y2 = 420;
		nckP->high = 24;
		nckP->words = L"￥648 * 1000";
		noClicks.push_back(nckP);
	}


	{	//支付方式窗口
		std::shared_ptr<NoClickPortrait> nckP(new NoClickPortrait);
		nckP->x1 = 570;
		nckP->x2 = 780;
		nckP->y1 = 120;
		nckP->y2 = 360;
		nckP->high = 24;
		noClicks.push_back(nckP);
	}
	{	//支付
		std::shared_ptr<Click> ckP(new Click);
		ckP->x1 = 580;
		ckP->x2 = 760;
		ckP->y1 = 380;
		ckP->y2 = 460;
		ckP->high = 48;
		ckP->words = L"支付";
		ckP->fun = [this]()->std::shared_ptr<Page> {
			this->pay();
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//支付方式窗口 - 支付宝
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 670;
		nckP->x2 = 750;
		nckP->y1 = 150;
		nckP->y2 = 180;
		nckP->high = 24;
		nckP->words = L"支付宝";
		noClicks.push_back(nckP);

		std::shared_ptr<NoClick> nck2P(new NoClick);
		nck2P->x1 = 640;
		nck2P->x2 = 670;
		nck2P->y1 = 150;
		nck2P->y2 = 180;
		nck2P->high = 24;
		nck2P->position = L"pages\\ico21.jpeg";
		noClicks.push_back(nck2P);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = 590;
		ckP->x2 = 630;
		ckP->y1 = 142;
		ckP->y2 = 182;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_way(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_way(p, 1);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//支付方式窗口 - 微信
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 670;
		nckP->x2 = 750;
		nckP->y1 = 200;
		nckP->y2 = 230;
		nckP->high = 24;
		nckP->words = L"微信";
		noClicks.push_back(nckP);

		std::shared_ptr<NoClick> nck2P(new NoClick);
		nck2P->x1 = 640;
		nck2P->x2 = 670;
		nck2P->y1 = 200;
		nck2P->y2 = 230;
		nck2P->high = 24;
		nck2P->position = L"pages\\ico22.jpeg";
		noClicks.push_back(nck2P);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = 590;
		ckP->x2 = 630;
		ckP->y1 = 192;
		ckP->y2 = 232;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_way(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_way(p, 2);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//支付方式窗口 - 花呗
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 670;
		nckP->x2 = 750;
		nckP->y1 = 250;
		nckP->y2 = 280;
		nckP->high = 24;
		nckP->words = L"花呗";
		noClicks.push_back(nckP);

		std::shared_ptr<NoClick> nck2P(new NoClick);
		nck2P->x1 = 640;
		nck2P->x2 = 670;
		nck2P->y1 = 250;
		nck2P->y2 = 280;
		nck2P->high = 24;
		nck2P->position = L"pages\\ico23.jpeg";
		noClicks.push_back(nck2P);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = 590;
		ckP->x2 = 630;
		ckP->y1 = 242;
		ckP->y2 = 282;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_way(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_way(p, 3);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}
	{	//支付方式窗口 - 原石
		std::shared_ptr<NoClick> nckP(new NoClick);
		nckP->x1 = 670;
		nckP->x2 = 750;
		nckP->y1 = 300;
		nckP->y2 = 330;
		nckP->high = 24;
		nckP->words = L"原石";
		noClicks.push_back(nckP);

		std::shared_ptr<NoClick> nck2P(new NoClick);
		nck2P->x1 = 640;
		nck2P->x2 = 670;
		nck2P->y1 = 300;
		nck2P->y2 = 330;
		nck2P->high = 24;
		nck2P->position = L"pages\\ico20.jpg";
		noClicks.push_back(nck2P);

		std::shared_ptr<ClickHandoverBool> ckP(new ClickHandoverBool);
		ckP->x1 = 590;
		ckP->x2 = 630;
		ckP->y1 = 292;
		ckP->y2 = 332;
		ckP->high = 24;
		ckP->position = L"pages\\ico18.jpg";
		ckP->positionTrue = L"pages\\ico19.jpg";
		ckP->funTrue = [this]()->std::shared_ptr<Page> {
			this->set_way(nullptr, 0);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		ClickHandoverBool* p = ckP.get();
		ckP->funFalse = [this, p]()->std::shared_ptr<Page> {
			this->set_way(p, 4);
			PageAdministrator::FlagDynamicFlush = true;	//强制刷新
			return nullptr; };
		clicks.push_back(ckP);
	}


}