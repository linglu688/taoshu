#pragma once
#ifndef MYREADTXT_INCLUDED
#define MYREADTXT_INCLUDED

#include"MyUtility.h"

#include<fstream>
#include<iostream>
#include<unordered_map>
#include<string>
#include<vector>

/*
例子
.h
class A{

	friend class Read<A>;
	friend class Write<A>;
private:
	string name;
	const std::string get_name(){return name;}
	void set_name(std::string str){name = str;}
	void read(std::ifstream& file){Read<A>::Fun(*this,file);}
	void write(std::ofstream& file) const {Write<A>::Fun(*this,file);}
}

.cpp
Read<A>::Hash_type<A> Read<A>::Hash = {
{std::string("name"),&A::get_name}
};
Write<A>::Hash_type<A> Write<A>::Hash = {
{std::string("name"),&A::set_name}
};

*/

/********************************     Read        *******************************/

template<typename Type>
class Read {
public:
	using Hash_type = std::unordered_map<std::wstring, void (Type::*)(std::wstring)>;
	static Hash_type Hash;
	static void Fun(Type& c, std::wifstream& file);
};

template<typename Type>
void Read<Type>::Fun(Type& c, std::wifstream& file) {
	file.imbue(loc);
	if (!file) { return; }								/*文件未打开*/
	std::wstring str;
	wchar_t ch = L'\0';
	while (ch != L']' && file.get(ch)) { ; }			/*寻找开始标记*/
	while (ch != L'\n' && file.get(ch)) { ; }			/*寻找下一行*/
	while (true) {
		str.clear();									/*每次都清空*/
		while (file.get(ch)) {
			if (ch == L'[') { return; }					/*读到了下一个该读取的内容,直接结束*/
			else if (ch != L';' && ch != L'\n') { str.push_back(ch); }
			else { break; }								/*遇到\n和,结束*/
		}
		if (str.size() != 0) {							/*遇到的不是空行，进入处理*/
			int k = 0;
			while (k < str.size() && str[k] != L'=') { k++; }/*寻找 = 号*/
			if (k == str.size()) { return; }			/*没有找到=，结束*/
			int i = k - 1;
			while (i > -1 && str[i] == L' ') { i--; }	/*寻找等号前面的空格*/
			std::wstring index = str.substr(0, i + 1);
			if (Read<Type>::Hash.find(index) == Read<Type>::Hash.end()) { return; }/*没有找到匹配函数*/
			int j = k + 1;
			while (j < str.size() && str[j] == L' ') { j++; }/*寻找等号后面的空格*/
			std::wstring temp = str.substr(j);
			auto p = Read<Type>::Hash[index];			/*获取对应的函数*/
			(c.*p)(temp);								/*调用赋值*/
		}
		while (ch != L'\n' && file.get(ch)) { ; }		/*处理注释*/
		if (file.eof()) { return; }						/*结尾返回*/
	}
}


/********************************     Write        *******************************/
/*
* wirte 约定 hash表第一个位置为 名字 = name
*/
template<typename Type>
class Write {
public:
	using Hash_type = std::vector<std::pair<std::wstring,std::wstring(Type::*)()const>>;
	static Hash_type Hash;
	static void Fun(const Type& c, std::wofstream& file);
};

template<typename Type>
void Write<Type>::Fun(const Type& c, std::wofstream& file) {
	file.imbue(loc);
	file << L"\n[";
	if (Write<Type>::Hash.size() > 0) {
		auto& p = Write<Type>::Hash.front().second;
		std::wstring name = (c.*p)();
		file << name;
	}
	file << L"]\n";
	for (auto& it : Write<Type>::Hash) {
		auto& p = it.second;
		std::wstring data = (c.*p)();
		std::wstring name = it.first;
		file << name << L" = " << data << L";\n";
	}
}

#endif
