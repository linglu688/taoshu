#pragma once
#include"MyReadTxt.h"
#include"MyUtility.h"
#include"MyBook.h"


#include <conio.h>
#include <graphics.h>

#include <functional>
#include <utility>
#include <vector>
#include <stack>
#include <exception>
#include <stdexcept>
#include <map>



/********************************* NoClick  Click  *************************************/
//先声明以下 让页面类使用
struct ClickBase;				//可点击基类
struct NoClick;					//不可点击类
struct NoClickDynamicValue;		//不可点击动态值类


/*****************************   Page    页面基类        ************************************/
class Page {
	//公有接口
public:
	virtual ~Page() noexcept = default;						//析构函数
	virtual void ShowPage();									//画面函数
	virtual std::shared_ptr<Page> DetectionClick(ExMessage& f);	//触发点击函数

//公有数据
public:
	std::vector<std::shared_ptr<ClickBase>> clicks;				//点击类数组
	std::vector<std::shared_ptr<NoClick>> noClicks;				//不可点击类数组
	bool dynamicFlag = false;									//动态标志
	int dynamicCount = 0;										//动态量
	int dynamicCountMax = 0;									//动态量最大值
};

/*****************************   DynamicPage    动态页面基类     ************************************/
/*
* 基类： 页面基类
* 重写： 画面函数，触发函数
* 新增： 动态点击数组，动态不可被点击数组
*/
class DynamicPage : public Page {
public:
	DynamicPage();														//默认构造函数
	~DynamicPage() noexcept = default;									//析构函数
	virtual void ShowPage() override;									//画面函数
	virtual std::shared_ptr<Page> DetectionClick(ExMessage& f) override;//触发点击函数
	void SetDynamicCount(int k);										//设置动态值
	std::vector<std::shared_ptr<ClickBase>> dynamicClicks;				//动态点击类数组
	std::vector<std::shared_ptr<NoClick>> dynamicNoClicks;				//动态不可点击类数组
};

/*设置动态值*/
inline void DynamicPage::SetDynamicCount(int k) {
	if (k > 0 ) { return; }												//正值 错误
	if (k < dynamicCountMax) { dynamicCount = dynamicCountMax;}			//值过小
	else { dynamicCount = k; }											//值正常
}


/**********************         ClickBase    点击基类         ******************************/
struct ClickBase {

//公有方法
public:
	~ClickBase() noexcept = default;			//
	virtual void Show() = 0;					//画面函数
	virtual void ShowDynamic(int k) = 0;		//动态画面函数

//公有数据
public:
	int x1 = 0, x2 = 0, y1 = 0, y2 = 0;			//矩形四坐标
	int high = 24;
	std::function<std::shared_ptr<Page>()> fun;//执行功能 即返回新页面
};


/*********************         Click    点击类         *********************************/
struct Click : public ClickBase {
/*
* 基类： 点击基类
* 重写： 触发函数
* 新增： 文字 和 图片
*/
//公有方法
public:
	~Click() noexcept = default;				//
	virtual void Show() override;				//画面函数
	virtual void ShowDynamic(int k) override;	//动态画面函数

//公有数据
public:
	std::wstring words;							//文字字符串
	std::wstring position;						//图片位置
};


/***********************        ClickS    点击类集合版         ************************/
struct ClickS : public Click {
/*
* 基类： 点击类
* 重写： 触发函数
* 新增： 触发数组
*/
//公有方法
public:
	ClickS();
	~ClickS() noexcept = default;				//
//公有数据
public:
	std::vector<std::function <std::shared_ptr<Page>()>> funs;
private:
	using  Click::fun;							//触发函数 变私有
};

/***********************     ClickHandover    点击切换类        ********************************/
struct ClickHandover : public ClickBase {
	/*
	* 基类： 点击基类
	* 重写： 画面函数，动态画面函数
	* 新增： 图片数组 字符串数组
	*/
	//公有方法
public:
	ClickHandover() noexcept = default;			//
	virtual void Show() override;				//画面函数
	virtual void ShowDynamic(int k) override;	//动态画面函数

//公有成员
public:
	int handoverCount = 0;						//当前
	int handoverCountMax = 0;					//最大
	std::vector<std::wstring> handoverWords;	//文字字符串
	std::vector<std::wstring> handoverPosition;	//图片位置
};


/******************     ClickHandoverBool    点击切换类（Bool）   ************************/
struct ClickHandoverBool : public Click {
/*
* 基类： 点击类
* 重写： 画面函数，动态画面函数
* 新增： 字符串2 图片2
*/
//公有方法
public:
	ClickHandoverBool();								//
	virtual void Show() override;						//画面函数
	virtual void ShowDynamic(int k) override;			//动态画面函数

//公有成员
public:
	bool flag = false;
	std::wstring wordsTrue;
	std::wstring positionTrue;
	std::function <std::shared_ptr<Page>()> funTrue;
	std::function <std::shared_ptr<Page>()> funFalse;
};


/***************    ImportDataBase     键盘输入的数据结构基类        ******************/
struct ImportDataBase {
	int maxCount = 0;							//最大输入
	bool flag = false;							//选择标志
	static unsigned flicker;					//
	//static std::locale loc;					//环境变量
	virtual void push(wchar_t&) = 0;			//写入字符
	virtual void pop()  = 0;					//弹出字符
	virtual ~ImportDataBase(){};
};


/*************     ImportData     键盘输入的数据结构(全部类型)        ******************/
/*
* 基类： 键盘输入的数据结构基类
* 重写： 写入字符 弹出字符
* 新增： 字符绑定
*/
struct ImportData : public ImportDataBase {
	std::wstring& w_words;						//输入绑定到一个string上面
	ImportData(std::wstring& wstr) : ImportDataBase(),w_words(wstr) {};
	virtual void push(wchar_t& wch) override {
		if ( w_words.size() < maxCount  && !std::iscntrl(wch, loc)) {
			w_words.push_back(wch);}
	}
	virtual void pop() override {
		if (w_words.size() > 0) {
			w_words.pop_back();}
	}
};

/***********************      ImportDataNumber     键盘输入的数据结构(数字)        ******************/
/*
* 基类： 键盘输入的数据结构基类
* 重写： 写入字符 弹出字符
* 新增： 数字绑定 
*/
struct ImportDataNumber : public ImportDataBase {
	int& w_num;
	std::wstring w_words;
	static constexpr int w_min = 0;
	static constexpr int w_max = 1000'000'000;
	ImportDataNumber(int& num) : ImportDataBase(),w_num(num) {
		if(num != w_min && num != w_max)
		w_words = std::to_wstring(num);
	};
	virtual void push(wchar_t& wch) override {
		if (w_words.size() < maxCount && std::isdigit(wch, loc)) {
			w_words.push_back(wch);
			w_num = w_words.empty() ? 0 : stoi(w_words);
		}
	}
	virtual void pop() override {
		if (w_words.size() > 0) {
			w_words.pop_back();
			w_num = w_words.empty() ? 0 : stoi(w_words);
		}
	}
};


/*************     ImportDataPassword     键盘输入的数据结构(密码)        **********/
/*
* 基类： 键盘输入的数据结构基类
* 重写： 写入字符 弹出字符
* 新增： 字符绑定
*/
struct ImportDataPassword : public ImportDataBase {
	std::wstring& w_words;						//输入绑定到一个string上面
	std::wstring password;						//密码
	ImportDataPassword(std::wstring& wstr) : ImportDataBase(), w_words(wstr) {};
	virtual void push(wchar_t& wch) override {
		if (w_words.size() < maxCount && !std::iscntrl(wch, loc)) {
			w_words.push_back(wch);
			password.push_back(L'*');
		}
	}
	virtual void pop() override {
		if (w_words.size() > 0) {
			w_words.pop_back();
			password.pop_back();
		}
	}
};


/***********************     ClickImportr    点击输入类        ********************************/
struct ClickImportr : public ClickBase {
/*
* 基类： 点击基类
* 重写： 画面函数，动态画面函数
* 新增： 输入数据结构
*/
//公有方法
public:
	ClickImportr(std::wstring& wstr) : importData(wstr){};
	virtual void Show() override;				//画面函数
	virtual void ShowDynamic(int k) override;	//动态画面函数
//公有成员
public:
	ImportData importData;						//输入数据
};


/***************     ClickImportrPassword  点击输入类(密码)        ************************/
struct ClickImportrPassword : public ClickBase {
/*
* 基类： 点击基类
* 重写： 画面函数，动态画面函数
* 新增： 输入数据结构
*/
//公有方法
public:
	ClickImportrPassword(std::wstring& wstr) : importData(wstr) {};
	virtual void Show() override;				//画面函数
	virtual void ShowDynamic(int k) override;	//动态画面函数
//公有成员
public:
	ImportDataPassword importData;				//输入数据

};


/************     ClickImportrNumber    点击输入类(数字)       ***********************/
struct ClickImportrNumber : public ClickBase {
/*
* 基类： 点击类
* 重写： 画面函数，动态画面函数
* 新增： 输入数据结构
* 隐藏： 基类words,position
*/
//公有方法
public:
	ClickImportrNumber(int& num) : importData(num) {};
	virtual void Show() override;				//画面函数
	virtual void ShowDynamic(int k) override;	//动态画面函数

//公有成员
public:
	ImportDataNumber importData;				//输入数据
};


/****************************         NoClick    不可点击类        *************************************/
struct NoClick {
	//公有方法
public:
	virtual void ShowDynamic(int k);			//动态画面函数
	virtual void Show();						//画面函数
	void ShowL();								//画面函数（靠右绘制 ）
	~NoClick() noexcept = default;				//析构
//公有数据
public:
	int x1 = 0, x2 = 0, y1 = 0, y2 = 0;			//矩形四坐标
	int high = 24;
	std::wstring words;							//文字字符串
	std::wstring position;						//图片位置
};


/****************************         NoClickL    不可点击类(左)        *************************************/
/*
* 基类： 不可点击类
* 重写： 画面函数，动态画面函数
*/
struct NoClickL : public NoClick {
//公有方法
public:
	virtual void ShowDynamic(int k);			//动态画面函数
	virtual void Show();						//画面函数
};


/***************************  NoClickDynamicValue  不可点击动态值类    *************************************/
struct NoClickDynamicValue : public NoClick {
/*
* 基类： 不可点击类
* 重写： 画面函数，动态画面函数
* 新增： 动态值计算函数 和 动态标志
*/
//公有方法
public:
	NoClickDynamicValue() noexcept = default;
	~NoClickDynamicValue() noexcept = default;
	virtual void Show() override;				//画面函数
	virtual void ShowDynamic(int k) override;	//动态画面函数

//公有成员
public:
	std::function<std::wstring()> dynamicWordsFun;	//动态字符函数
	bool dynamicWordsFlag = true;					//字符动态标志
};

/***************    NoClickDynamicWordsL    不可点击动态字符(左)     ********************/
struct NoClickDynamicWordsL : public NoClick {
/*
* 基类： 不可点击类
* 重写： 画面函数，动态画面函数
* 新增： 字符数组
*/
//公有方法
public:
	NoClickDynamicWordsL() noexcept = default;
	~NoClickDynamicWordsL() noexcept = default;
	virtual void Show() override;				//画面函数
	virtual void ShowDynamic(int k) override;	//动态画面函数

//公有成员
public:
	std::vector<std::wstring> wordsArr;			//文字字符串
	int count = 0;								//值
};




/********************      NoClick 不可点击头像类      *********************************/
/*
* 基类： 不可点击类
* 重写： 画面函数
*/
struct NoClickPortrait : public NoClick {
	//公有方法
public:
	NoClickPortrait() noexcept;
	~NoClickPortrait() noexcept = default;
	virtual void Show() override;					//画面函数
	virtual void ShowDynamic(int k) override;		//动态画面函数
};

/******************  PageAdministratorBase  页面管理基类    ***************************/
class PageAdministratorBase {
//公有方法
public:
	void Show();											//显示页面
	void Import();											//响应输入 未实现
	void initializeSetPicture();							//设置初始化
	static void SetCharImport(ImportDataBase* p);			//设置字符输入
	PageAdministratorBase();								
	~PageAdministratorBase() noexcept = default;
protected:
	virtual std::shared_ptr<Page> StackRollback();			//栈回退

	//公有数据
public:
	static bool FlagEnd;									//结束标志
	static bool FlagDynamicFlush;							//动态刷新标志 强制刷新
	static bool FlagNORollback;								//页面不回退标志
	static bool FlagStackRollback;							//栈回退标志
	static int dynamicCount;								//动态值
protected:
	static std::shared_ptr<Page> atPresentPage;				//当前页面指针
	static std::deque<std::shared_ptr<Page>> stackPagesP;	//页面指针栈
	static constexpr int stackCountMax = 10;				//栈最大
	static ImportDataBase* importrP;						//输入指针
};

//设置键盘输入
inline void PageAdministratorBase::SetCharImport(ImportDataBase* p) {
	if (importrP != nullptr) importrP->flag = false;		//关闭上个输入
	importrP = p;											//获取最新输入
	importrP->flag = true;									//设置字符输入
};														