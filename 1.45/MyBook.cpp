#include"MyBook.h"

/***************************************   book   书     ********************************/
/*read write函数*/
Read<Book>::Hash_type Read<Book>::Hash = {
	{std::wstring(L"书名"), &Book::set_name},
	{ std::wstring(L"价格"), &Book::set_price },
	{ std::wstring(L"图片3"), &Book::set_photo_200x150},
	{ std::wstring(L"图片1"), &Book::set_photo_100x75},
	{ std::wstring(L"图片2"), &Book::set_photo_140x105},
	{ std::wstring(L"销量"), &Book::set_sale},
	{ std::wstring(L"库存"), &Book::set_repertory},
	{ std::wstring(L"商户"), &Book::set_bookstoreId}
};

Write<Book>::Hash_type Write<Book>::Hash =  {
	{std::wstring(L"书名"), &Book::get_name},
	{ std::wstring(L"商户"), &Book::get_bookstoreId },
	{ std::wstring(L"价格"), &Book::get_price },
	{ std::wstring(L"销量"), &Book::get_sale },
	{ std::wstring(L"库存"),  &Book::get_repertory },
	{ std::wstring(L"图片1"), &Book::get_photo_100x75 },
	{ std::wstring(L"图片2"), &Book::get_photo_140x105 },
	{ std::wstring(L"图片3"),& Book::get_photo_200x150 }
};


/**********************************   Bookstore  书店 ************************************/
/*read 函数*/
Read<Bookstore>::Hash_type Read<Bookstore>::Hash = {
		{std::wstring(L"name"), & Bookstore::set_name},
		{ std::wstring(L"position"),&Bookstore::set_bookPosition },
		{ std::wstring(L"id"), &Bookstore::set_id },
		{ std::wstring(L"portrait"), &Bookstore::set_portrait }
};


/*******************************   BookstoreAdministrator  书店管理 ************************************/
/*构造函数*/
BookstoreAdministrator::BookstoreAdministrator()
	: shopping(*SingletonPattern<Shopping>::Get()) {
	if (bookstores.size() == 0) {
		{	//读取每个书店
			std::wifstream file(cs_position);
			while (!file.eof()) {
				Bookstore bs(file);						//初始化每一个书店
				if (bs.name != L"")
					bookstores.emplace(bs.id, bs);		//存进书店表
			}
		}
		{	//结算上次购物记入
			Book book;								
			std::set<int> tempSet;
			std::wifstream file2;						//这里显式打开 是为了一会清空
			file2.open(cs_upperShopping);
			while (!file2.eof()) {
				book.read(file2);
				if (book.name != L"") {
					std::set<Book>::iterator it;
					it = bookstores[book.bookstoreId].books.find(book);
					it->sale += book.sale;				//销量累加
					it->repertory -= book.sale;			//库存减少
					tempSet.insert(book.bookstoreId);	//加入写入表
				}
			}
			for (auto& it : tempSet) {
				std::wofstream file3(bookstores[it].bookPosition);
				for (auto& it2 : bookstores[it].books) {
					it2.write(file3);
				}
			}
			file2.close();								//显式关闭
			std::ofstream file4;
			file4.open(cs_upperShopping);				//显式打开	上一次购物记入文件
			file4.close();								//显式关闭 清空文件
		}
		{	//初始化图书表
			std::back_insert_iterator<std::vector<Book>> booksBack(books);
			for (auto& bs : bookstores) {				//复制每个书店图书进图书表
				std::copy(bs.second.books.begin(), bs.second.books.end(), booksBack);
			}
		}
	}	//if
	shoppingFlush();	//刷新购物车
}

//查找函数
//x 从0开始 2，3 表示从第2个开始的3个 下标 2 3 4 
int BookstoreAdministrator::BooksFind(Book& book1, 
	Book& book2, std::vector<Book>& bks, int x, int y) {
	int k = 0;
	static std::vector<std::wstring> strs;
	strs.clear();
	int j = -1;
	int size = book1.name.size();
	for (int i = 0; i < size; i++) {
		if (j < 0) {
			if(book1.name[i] != ' '){
				j = i;
				i--;
			}
			continue;
		}
		if (i == size - 1) {
			if (book1.name[i] != ' ')
				strs.push_back(book1.name.substr(j, i - j + 1));
		}
		else {
			if (book1.name[i + 1] == ' ') {
				strs.push_back(book1.name.substr(j, i - j + 1));
				j = -1;
			}
		}
	}


	for (auto& it : books) {

		//如果名字不是空 且匹配不到子串 
		bool flag = false;
		if (strs.size() == 0) { flag = false;}
		else{
			flag = true;
			for (auto& str : strs) {
				flag &= (it.name.find(str) == std::string::npos);
			}
		}
		if (flag)
			continue;

		if (book1.price > it.price || book2.price < it.price)
			continue;
		if (book1.repertory > it.repertory || book2.repertory < it.repertory)
			continue;
		if (book1.sale > it.sale || book2.sale < it.sale)
			continue;
		//k++;
		if (k++ >= x) {
			if (y-- > 0) {bks.push_back(it);}
			else {break;}
		}
	}
	return k < x ? 0 : k - x;
}
