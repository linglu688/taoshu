#include <conio.h>
#include <graphics.h>
#include "MyBook.h"
#include "MyUtility.h"
#include "MyPage.h"
#include "MySingletonPattern.h"
#include <thread>


//
//#define CRTDBG_MAP_ALLOC  
//#include <stdlib.h>  
//#include <crtdbg.h>  
//
//
////加这个才会定位到第几行内存泄漏，并且泄漏多少字节，这个只针对new申请的空间生效
//#define new new(_CLIENT_BLOCK,__FILE__,__LINE__)


int main()
{
	 //创建绘图窗口





	


	







	initgraph(800, 480);
	SingletonPattern<PageAdministrator>::Create(1);		//构造 无参：用户 有参：管理员
	PageAdministrator& pa =
		*SingletonPattern<PageAdministrator>::Get();	//页面管理类




	std::thread f1(&PageAdministrator::Import, &pa);	//响应鼠标
	f1.detach();										//分离
	pa.Show();											//显示画面
	Sleep(10);
	SingletonPattern<PageAdministrator>::Destroy();		//析构
	closegraph();

	


	//auto& bka = *SingletonPattern<BookstoreAdministrator>::Create();
	//std::ofstream file;
	//for (auto& it : bka.bookstores) {					//遍历每个商家
	//	file.open(it.second.bookPosition);				
	//	for (auto it2 : it.second.books) {				//遍历每个商家的书
	//		it2.write(file);							//写回文件
	//	}
	//	file.close();
	//}

	//	//下面这两个是指警告信息的输出模式，下面是指输出到控制台。
	//		//没有设置则信息是输出到VS的自带的输出框里
	//_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
	//_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT);
	//_CrtDumpMemoryLeaks();

	return 0;

}