#pragma once

#include<type_traits>

/***************************      单例模式模板      *********************************/
template<typename T>
class SingletonPattern {
public:
	template<typename... Type>
	static T*& Create(Type&& ... value);
	static T*& Get();
	static void Destroy();
private:
	SingletonPattern() = default;
	virtual ~SingletonPattern() = default;
	SingletonPattern(const SingletonPattern&) = delete;
	SingletonPattern& operator = (const SingletonPattern&) = delete;
private:
	static T* p;
};
template<typename T>
T* SingletonPattern<T>::p = nullptr;


//初始化函数
template<typename T>
template<typename... Type>
inline T*& SingletonPattern<T>::Create(Type&& ... value) {
	if (p == nullptr) {
		p = new T(std::forward<Type>(value)...);
	}
	return p;
}

//得到指针
//如果没有初始化 就进行默认初始化
template<typename T>
inline T*& SingletonPattern<T>::Get() {
	if (p == nullptr) {
		p = new T;
	}
	return p;
}

//析构
//nullptr 不会报错
template<typename T>
inline void SingletonPattern<T>::Destroy() {
	if(p != nullptr)
		delete p;
	p = nullptr;
}

