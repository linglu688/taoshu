﻿#pragma once
#include <string>
#include <codecvt>
#include <locale>
#include <conio.h>
#include <graphics.h>

static std::locale loc("zh_CN");

/********************************   编码转化    **********************************/

//std::string תΪ std::wstring( utf-8 --> wchar )
inline std::wstring utf8_to_wstr(const std::string& src)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
	return converter.from_bytes(src);
}

//std::wstringתΪstd::string(wchar --> utf-8)
inline std::string wstr_to_utf8(const std::wstring& src)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> convert;
	return convert.to_bytes(src);
}


//utf-8תgbk
inline std::string utf8_to_gbk(const std::string& str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t> > conv;
	std::wstring tmp_wstr = conv.from_bytes(str);

	//GBK locale name in windows
	const char* GBK_LOCALE_NAME = ".936";
	std::wstring_convert<std::codecvt_byname<wchar_t, char, mbstate_t>> convert(new std::codecvt_byname<wchar_t, char, mbstate_t>(GBK_LOCALE_NAME));
	return convert.to_bytes(tmp_wstr);
}

//gbkתutf-8
inline std::string gbk_to_utf8(const std::string& str)
{
	//GBK locale name in windows
	const char* GBK_LOCALE_NAME = ".936";
	std::wstring_convert<std::codecvt_byname<wchar_t, char, mbstate_t>> convert(new std::codecvt_byname<wchar_t, char, mbstate_t>(GBK_LOCALE_NAME));
	std::wstring tmp_wstr = convert.from_bytes(str);

	std::wstring_convert<std::codecvt_utf8<wchar_t>> cv2;
	return cv2.to_bytes(tmp_wstr);

}

//gbkתstd::wstring
inline std::wstring gbk_to_wstr(const std::string& str)
{
	//GBK locale name in windows
	const char* GBK_LOCALE_NAME = ".936";
	std::wstring_convert<std::codecvt_byname<wchar_t, char, mbstate_t>>
		convert(new std::codecvt_byname<wchar_t, char, mbstate_t>(GBK_LOCALE_NAME));
	return convert.from_bytes(str);
}

//自己模仿的
inline std::string wstr_to_gbk(const std::wstring& wstr)
{
	//GBK locale name in windows
	const char* GBK_LOCALE_NAME = ".936";
	std::wstring_convert<std::codecvt_byname<wchar_t, char, mbstate_t>>
		convert(new std::codecvt_byname<wchar_t, char, mbstate_t>(GBK_LOCALE_NAME));
	return convert.to_bytes(wstr);
}







/********************************      画面接口      *************************************/
namespace show {
	static int lastHigh;
	static COLORREF lastRgb;

	//修改字高
	inline void setHigh(int high) {
		if (high != lastHigh) {						//字体需要修改
			lastHigh = high;
			LOGFONT f;
			gettextstyle(&f);						// 获取当前字体设置
			f.lfHeight = high;
			settextstyle(&f);						// 设置字体样式 临时
		}
	}

	//图片
	inline void showPhoto(std::string str, int x1, int x2, int y1, int y2) {
		IMAGE img1;
		std::wstring wstr = gbk_to_wstr(str);
		loadimage(&img1, wstr.c_str());
		putimage(x1, y1, &img1);
	};

	inline void showPhoto(std::wstring wstr, int x1, int x2, int y1, int y2) {
		IMAGE img1;
		loadimage(&img1, wstr.c_str());
		putimage(x1, y1, &img1);
	};

	//文字剧中
	inline void showWords(std::string str, int x1, int x2, int y1, int y2, int high = 24) {
		setHigh(high);
		std::wstring wstr = gbk_to_wstr(str);
		RECT r = { x1, y1, x2, y2 };
		drawtext(wstr.c_str(), &r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	};
	inline void showWords(std::wstring wstr, int x1, int x2, int y1, int y2, int high = 24) {
		setHigh(high);
		RECT r = { x1, y1, x2, y2 };
		drawtext(wstr.c_str(), &r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	};
	//文字左
	inline void showWordsL(std::string str, int x1, int x2, int y1, int y2, int high = 24) {
		setHigh(high);
		std::wstring wstr = gbk_to_wstr(str);
		RECT r = { x1, y1, x2, y2 };
		drawtext(wstr.c_str(), &r, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	};

	//文字左
	inline void showWordsL(std::wstring wstr, int x1, int x2, int y1, int y2, int high = 20) {
		setHigh(high);
		RECT r = { x1, y1, x2, y2 };
		drawtext(wstr.c_str(), &r, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	};

	//头像框
	inline void showPortrait(int x1, int x2, int y1, int y2) {
		rectangle(x1, y1, x2, y2);
	};

	//填充
	inline void showFill(int x1, int x2, int y1, int y2, COLORREF rgb = LIGHTGRAY) {
		if (lastRgb != rgb) {
			setfillcolor(rgb);
			lastRgb = rgb;
		}
		solidrectangle(x1, y1, x2, y2);
	};



	inline void initSet() {
		LOGFONT f;
		gettextstyle(&f);								//获取当前字体设置
		f.lfHeight = 24;								//设置字体高度为 96
		f.lfWeight = 600;								//粗度
		f.lfQuality = PROOF_QUALITY;					//正稿质量
		_tcscpy_s(f.lfFaceName, _T("楷体"));			//设置字体
		setbkcolor(WHITE);								//背景白色
		settextcolor(BLACK);							//黑色字
		setfillcolor(BLACK);							//设置填充黑色
		setbkmode(TRANSPARENT);							//文字背景透明 即不填充背景
		settextstyle(&f);								//设置字体样式
		setlinecolor(BLACK);
	}
}
