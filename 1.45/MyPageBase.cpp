#include"MyPageBase.h"
/*****************************   Page    页面基类        ************************************/
/*画面函数*/
void Page::ShowPage() {
	cleardevice();								//清空屏幕
	for (auto& it : noClicks) { it->Show(); }	//显示不可点击类的图像
	for (auto& it : clicks) { it->Show(); }		//显示点击类的图像
}

/*检测点击函数*/
std::shared_ptr<Page> Page::DetectionClick(ExMessage& f) {
	bool flag = 0;
	for (auto& it : clicks) {
		if (f.x > it->x1 && f.x < it->x2 && f.y > it->y1 && f.y < it->y2) {	//检测位置
			return std::shared_ptr<Page>((it->fun)());}
	}
	return nullptr;								//返回空页
}

/*****************************  DynamicPage  ******************************/
/*构造函数*/
DynamicPage::DynamicPage() : Page() { dynamicFlag = true; };

/*画面函数*/
void DynamicPage::ShowPage() {
	cleardevice();								//清空屏幕
	for (auto& it : dynamicClicks) {			//显示动态点击类的图像
		it->ShowDynamic(dynamicCount);}
	for (auto& it : dynamicNoClicks) {			//显示动态不可点击类的图像
		it->ShowDynamic(dynamicCount);}	
	for (auto& it : clicks) { it->Show(); }		//显示点击类的图像
	for (auto& it : noClicks) { it->Show(); }	//显示不可点击类的图像
}

/*检测点击函数*/
std::shared_ptr<Page> DynamicPage::DetectionClick(ExMessage& f) {
	bool flag = 0;
	
	for (auto& it : clicks) {
		if (f.x > it->x1 && f.x < it->x2 && f.y > it->y1 && f.y < it->y2) {	//检测位置
			return std::shared_ptr<Page>((it->fun)());}
	}
	int k = dynamicCount;//获取动态值
	for (auto& it : dynamicClicks) {
		if (f.x > it->x1 && f.x < it->x2 && f.y > it->y1 + k && f.y < it->y2 + k) {	//检测位置
			return std::shared_ptr<Page>((it->fun)());}
	}
	return nullptr;								//返回空页
}

/****************************         Click    点击类         ********************/
/*画面函数*/
void  Click::Show() {
	if (words.size() != 0) {					//字符串不是空
		show::showWords(words, x1, x2, y1, y2, high);
	}
	if (position.size() != 0) {					//图片地址 不是空 
		show::showPhoto(position, x1, x2, y1, y2);
	}
}

/*动态画面函数*/
void  Click::ShowDynamic(int k) {						//k 动态偏差值
	int k1 = y1 + k;										//计算偏差后坐标
	int k2 = y2 + k;
	if (words.size() != 0 && k2 > 0 && k1 < 480) {			//字符串不是空
		show::showWords(words, x1, x2, k1, k2, high);
	}
	if (position.size() != 0 && k2 > 0 && k1 < 480) {		//图片地址 不是空 
		show::showPhoto(position, x1, x2, k1, k2);
	}
}
/***********************        ClickS    点击类集合版         ************************/
/*构造函数*/
ClickS::ClickS() : Click() {
	fun = [this]()->std::shared_ptr<Page> {
		std::shared_ptr<Page> p;
		std::shared_ptr<Page> temp;
		bool flag = true;
		for (auto& fun : this->funs) {							//遍历funs
			temp = fun();										//执行并获取返回指针
			if (temp != nullptr && flag) {						//只保存第一个非nullptr指针
				p = temp;
				flag = false;
			}
		}
		return p;												
	};
}


/***********************     ClickHandover    点击切换类        ********************************/
/*画面函数*/
void  ClickHandover::Show() {
	if (handoverWords.size() != 0) {							//字符串不是空
		show::showWords(handoverWords[handoverCount], x1, x2, y1, y2, high);
	}
	if (handoverPosition.size() != 0) {							//图片地址 不是空 
		show::showPhoto(handoverPosition[handoverCount], x1, x2, y1, y2);
	}
}

/*动态画面函数*/
void ClickHandover::ShowDynamic(int k) {						//k 动态偏差值
	int k1 = y1 + k;											//计算偏差后坐标
	int k2 = y2 + k;
	if (handoverWords.size() != 0 && k2 > 0 && k1 < 480) {		//字符串不是空
		show::showWords(handoverWords[handoverCount], x1, x2, k1, k2, high);
	}
	if (handoverPosition.size() != 0 && k2 > 0 && k1 < 480) {	//图片地址 不是空 
		show::showPhoto(handoverPosition[handoverCount], x1, x2, k1, k2);
	}
}


/******************     ClickHandoverBool    点击切换类（Bool）   ************************/
/*构造函数*/
ClickHandoverBool::ClickHandoverBool() : Click() {
	fun = [this]()->std::shared_ptr<Page> {
		return this->flag ? this->funTrue() : this->funFalse();
	};
}

/*画面函数*/
void  ClickHandoverBool::Show() {
	if (words.size() != 0) {					//字符串不是空
		flag ? show::showWords(wordsTrue, x1, x2, y1, y2, high)
		: show::showWords(words, x1, x2, y1, y2, high);
	}
	if (position.size() != 0) {					//图片地址 不是空 
		flag ? show::showPhoto(positionTrue, x1, x2, y1, y2)
		: show::showPhoto(position, x1, x2, y1, y2);
	}
}

/*动态画面函数*/
void ClickHandoverBool::ShowDynamic(int k) {						//k 动态偏差值
	int k1 = y1 + k;										//计算偏差后坐标
	int k2 = y2 + k;
	if (words.size() != 0 && k2 > 0 && k1 < 480) {			//字符串不是空
		flag ? show::showWords(wordsTrue, x1, x2, k1, k2, high)
		: show::showWords(words, x1, x2, k1, k2, high);
	}
	if (position.size() != 0 && k2 > 0 && k1 < 480) {		//图片地址 不是空 
		flag ? show::showPhoto(positionTrue, x1, x2, k1, k2)
		: show::showPhoto(position, x1, x2, k1, k2);
	}
}




/***********************     ClickImportr    点击输入类        ****************************/
/*画面函数*/
void  ClickImportr::Show() {
	show::showFill(x1, x2, y1, y2, LIGHTGRAY);		//绘制输入填充灰色
	if (importData.w_words.size() != 0) {							//字符串不是空
		show::showWordsL(importData.w_words, x1, x2, y1, y2,high);
	}
	importData.flicker++;
	if (importData.flag == true && importData.flicker % 128 < 32) {
		int width = textwidth(importData.w_words.c_str()) + x1;
		width > x2 ? width = x2 : 0;
		line(width + 2, y1 + 2, width + 2, y2 - 2);
	}
	if (importData.flag == true) {
		show::showPortrait(x1, x2, y1, y2);
	}
}

/*动态画面函数*/
void  ClickImportr::ShowDynamic(int k) {					//k 动态偏差值
	int k1 = y1 + k;										//计算偏差后坐标
	int k2 = y2 + k;
	show::showFill(x1, x2, k1, k2, LIGHTGRAY);				//绘制输入填充灰色
	if (importData.w_words.size() != 0 && k2 > 0 && k1 < 480) {			//字符串不是空
		show::showWordsL(importData.w_words, x1, x2, k1, k2, high);
	}
	importData.flicker++;
	if (importData.flag == true && importData.flicker % 128 < 32) {
		int width = textwidth(importData.w_words.c_str()) + x1;
		width > x2 ? width = x2 : 0;
		line(width + 2, k1 + 2, width + 2, k2 - 2);
	}
	if (importData.flag == true) {
		show::showPortrait(x1, x2, k1, k2);
	}
}


/***************     ClickImportrPassword  点击输入类(密码)        ************************/
/*画面函数*/
void  ClickImportrPassword::Show() {
	show::showFill(x1, x2, y1, y2, LIGHTGRAY);		//绘制输入填充灰色
	if (importData.w_words.size() != 0) {							//字符串不是空
		show::showWordsL(importData.password, x1, x2, y1, y2, high);
	}
	importData.flicker++;
	if (importData.flag == true && importData.flicker % 128 < 32) {
		int width = textwidth(importData.password.c_str()) + x1;
		width > x2 ? width = x2 : 0;
		line(width + 2, y1 + 2, width + 2, y2 - 2);
	}
	if (importData.flag == true) {
		show::showPortrait(x1, x2, y1, y2);
	}
}

/*动态画面函数*/
void ClickImportrPassword::ShowDynamic(int k) {					//k 动态偏差值
	int k1 = y1 + k;										//计算偏差后坐标
	int k2 = y2 + k;
	show::showFill(x1, x2, k1, k2, LIGHTGRAY);				//绘制输入填充灰色
	if (importData.w_words.size() != 0 && k2 > 0 && k1 < 480) {			//字符串不是空
		show::showWordsL(importData.password, x1, x2, k1, k2, high);
	}
	importData.flicker++;
	if (importData.flag == true && importData.flicker % 128 < 32) {
		int width = textwidth(importData.password.c_str()) + x1;
		width > x2 ? width = x2 : 0;
		line(width + 2, k1 + 2, width + 2, k2 - 2);
	}
	if (importData.flag == true) {
		show::showPortrait(x1, x2, k1, k2);
	}
}


/***********************     ClickImportrNumber    点击输入类(数字)        ****************************/
/*画面函数*/
void  ClickImportrNumber::Show() {
	show::showFill(x1 - 2, x2 + 1, y1 - 2, y2 + 1, LIGHTGRAY);		//绘制输入填充灰色
	if (importData.w_words.size() != 0) {							//字符串不是空
		show::showWordsL(importData.w_words, x1, x2, y1, y2, high);
	}
	importData.flicker++;
	if (importData.flag == true && importData.flicker % 128 < 32) {
		int width = textwidth(importData.w_words.c_str()) + x1;
		width > x2 ? width = x2 : 0;
		line(width + 2, y1 + 2, width + 2, y2 - 2);
	}
}

/*动态画面函数*/
void  ClickImportrNumber::ShowDynamic(int k) {				//k 动态偏差值
	int k1 = y1 + k;										//计算偏差后坐标
	int k2 = y2 + k;
	show::showFill(x1 - 2, x2 + 1, k1 - 2, k2 + 1, LIGHTGRAY);			//绘制输入填充灰色
	if (importData.w_words.size() != 0 && k2 > 0 && k1 < 480) {			//字符串不是空
		show::showWordsL(importData.w_words, x1, x2, k1, k2, high);
	}
	importData.flicker++;
	if (importData.flag == true && importData.flicker % 128 < 32) {
		int width = textwidth(importData.w_words.c_str()) + x1;
		width > x2 ? width = x2 : 0;
		line(width + 2, k1 + 2, width + 2, k2 - 2);
	}
}


/***********************         NoClick    不可点击类      *********************************/

/*画面函数*/
void  NoClick::Show() {
	if (words.size() != 0) { show::showWords(words, x1, x2, y1, y2, high); }
	if (position.size() != 0) { show::showPhoto(position, x1, x2, y1, y2); }
}

/*画面函数靠左显示*/
void  NoClick::ShowL() {
	if (words.size() != 0) { show::showWords(words, x1, x2, y1, y2 ,high); }
	if (position.size() != 0) { show::showPhoto(position, x1, x2, y1, y2); }
}

/*动态画面函数*/
void  NoClick::ShowDynamic(int k) {
	int k1 = y1 + k;
	int k2 = y2 + k;
	if (words.size() != 0 && k2 > 0 && k1 < 480) {			//字符串不是空
		show::showWords(words, x1, x2, k1, k2, high);
	}
	if (position.size() != 0 && k2 > 0 && k1 < 480) {		//图片地址 不是空 
		show::showPhoto(position, x1, x2, k1, k2);
	}
}


/***********************         NoClickL    不可点击类(左)      ***********************/
/*画面函数*/
void  NoClickL::Show() {
	if (words.size() != 0) { show::showWordsL(words, x1, x2, y1, y2, high); }
	if (position.size() != 0) { show::showPhoto(position, x1, x2, y1, y2); }
}

/*动态画面函数*/
void  NoClickL::ShowDynamic(int k) {
	int k1 = y1 + k;
	int k2 = y2 + k;
	if (words.size() != 0 && k2 > 0 && k1 < 480) {			//字符串不是空
		show::showWordsL(words, x1, x2, k1, k2, high);
	}
	if (position.size() != 0 && k2 > 0 && k1 < 480) {		//图片地址 不是空 
		show::showPhoto(position, x1, x2, k1, k2);
	}
}


/**************    NoClickDynamicValue    不可点击动态值类     *****************/

/*画面函数*/
void  NoClickDynamicValue::Show() {
	if (dynamicWordsFlag)
		words = dynamicWordsFun();
	NoClick::Show();
}

/*动态画面函数*/
void  NoClickDynamicValue::ShowDynamic(int k) {
	if (dynamicWordsFlag)
		words = dynamicWordsFun();
	NoClick::ShowDynamic(k);
}

/***************    NoClickDynamicWordsL    不可点击动态字符(左)     ********************/
/*画面函数*/
void  NoClickDynamicWordsL::Show() {
	if (wordsArr.size() != 0) {							//字符串不是空
		show::showWordsL(wordsArr[count], x1, x2, y1, y2, high);
	}
}

/*动态画面函数*/
void NoClickDynamicWordsL::ShowDynamic(int k) {				//k 动态偏差值
	int k1 = y1 + k;										//计算偏差后坐标
	int k2 = y2 + k;
	if (wordsArr.size() != 0 && k2 > 0 && k1 < 480) {		//字符串不是空
		show::showWordsL(wordsArr[count], x1, x2, k1, k2, high);
	}
}


/*****************************  NoClick 不可点击头像类   *************************************/
/*画面函数*/
void  NoClickPortrait::Show() {
	show::showPortrait(x1 - 1, x2, y1 - 1, y2);					//绘制边框
	if (position.size() != 0) {
		show::showPhoto(position, x1, x2, y1, y2);
	}
}

/*动态画面函数*/
void  NoClickPortrait::ShowDynamic(int k) {
	int k1 = y1 + k;
	int k2 = y2 + k;
	if (position.size() != 0 && k2 > 0 && k1 < 480) {			//图片地址 不是空
		show::showPortrait(x1 - 1, x2, k1 - 1, k2);				//绘制边框
		show::showPhoto(position, x1, x2, k1, k2);
	}
}

/*默认构造*/
NoClickPortrait::NoClickPortrait() noexcept :NoClick() {}


/***********************      ImportData     键盘输入的数据结构        ******************/
unsigned ImportDataBase::flicker = 0;
//std::locale ImportDataBase::loc;


/*************************  PageAdministratorBase  页面管理基类    ***************************/
bool PageAdministratorBase::FlagEnd = false;							//结束标志
bool PageAdministratorBase::FlagDynamicFlush = false;					//动态刷新标志 强制刷新
bool PageAdministratorBase::FlagNORollback = false;						//不退标志
bool PageAdministratorBase::FlagStackRollback = false;					//栈回退标志
int PageAdministratorBase::dynamicCount = 0;							//动态值
std::shared_ptr<Page> PageAdministratorBase::atPresentPage;				//当前页面指针
std::deque<std::shared_ptr<Page>> PageAdministratorBase::stackPagesP;	//页面指针栈
ImportDataBase* PageAdministratorBase::importrP = nullptr;				//键盘输入指针


/*画面函数*/
void PageAdministratorBase::Show() {
	std::shared_ptr<Page> p;
	while (1) {
		if (FlagEnd) { return; }							//退出标志
		if (p != atPresentPage || p->dynamicFlag) {			//静态页切换 或者是动态页
			p = atPresentPage;
			BeginBatchDraw();								//开始
			p->ShowPage();									//显示画面
			EndBatchDraw();									//结束
		}
		else if (FlagDynamicFlush) {						//强制刷新
			FlagDynamicFlush = false;
			p = atPresentPage;
			BeginBatchDraw();								//开始
			p->ShowPage();									//显示画面
			EndBatchDraw();									//结束
		}
		Sleep(10);
	}
}


//输入函数
void PageAdministratorBase::Import() {
	std::shared_ptr<Page> pp;											//页面指针
	ImportDataBase* &imp = importrP;									//输入指针
	ExMessage f;
	while (1) {
		if (FlagEnd) { return; }										//退出标志被设置 结束函数
		if (peekmessage(&f, EM_MOUSE | EM_CHAR)) {						//获取鼠标和键盘
			if (f.message == WM_LBUTTONDOWN) {							//鼠标点击
				std::shared_ptr<Page> pp = atPresentPage->DetectionClick(f);
				if (FlagStackRollback) {								//触发页面回退
					pp = StackRollback();}								//获取回退的页指针
				if (pp != nullptr) {
					if (stackPagesP.size() > stackCountMax) {
						stackPagesP.pop_front();}						//栈满了
					if (FlagNORollback) {
						FlagNORollback = false;}						//本次切换页不进栈
					else {
						stackPagesP.push_back(atPresentPage);}			//否则上一个页进栈
					atPresentPage = pp;									//页面指针切换
					if (imp != nullptr) {
						imp->flag = false;								//关闭上个输入
						imp = nullptr;}									//打断输入
				}
			}
			else if (f.message == WM_MOUSEWHEEL) {						//鼠标滚轮
				dynamicCount = atPresentPage->dynamicCount + f.wheel /4;//检测变化
				if (dynamicCount != atPresentPage->dynamicCount && dynamicCount <= 0
					&& dynamicCount >= atPresentPage->dynamicCountMax) {//值是否发生变化且变化正常
					atPresentPage->dynamicCount = dynamicCount;			//更新动态值
				}
			}
			else if (f.message == WM_CHAR) {
				if (imp != nullptr) {
					if (f.ch == L'\b') {imp->pop();}
					else {imp->push(f.ch);}
				}
			}

		}
		else{ Sleep(10); }
	}
}

/*默认构造*/
PageAdministratorBase::PageAdministratorBase() {
	initializeSetPicture();							//设置画面显示属性
}

/*设置初始化*/
void PageAdministratorBase::initializeSetPicture() {
	show::initSet();
}

//回退函数
inline std::shared_ptr<Page> PageAdministratorBase::StackRollback() {
	FlagStackRollback = false;
	std::shared_ptr<Page> p;
	while (!stackPagesP.empty()) {
		p = stackPagesP.back();
		stackPagesP.pop_back();
		if (p != nullptr) {
			break;}}
	return p;										//不保证可以回退成功
}