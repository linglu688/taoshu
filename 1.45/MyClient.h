#pragma once
#include"MyReadTxt.h"
#include"MySingletonPattern.h"

#include<vector>
#include<unordered_map>
#include<string>
#include<fstream>


/*************************************          提前声明         *******************************/
class Consumer;												//顾客
class ShoppingRecord;										//消费计入


/*******************    ShoppingRecord           购物记录  数据    ******************/
//消费记录的数据结构
struct ShoppingRecordDate {
	friend class Read<ShoppingRecordDate>;					//读功能
	friend class Write<ShoppingRecordDate>;					//写功能
	friend class ShoppingRecord;							//消费记录类

public:
	std::wstring name;										//购买书的名字
	std::wstring time;										//购买的时间
	std::wstring bookstoreName;								//商户名字
	int sale = 0;											//购买的价格
	int quantity = 1;										//购买的数量
	int bookstoreId = -1;									//购买商家的id
	int consumerId = -1;									//顾客id

private:
	void set_name(std::wstring str) { name = str; };
	void set_time(std::wstring str) { time = str; };
	void set_bookstoreName(std::wstring str) { bookstoreName = str; };
	void set_sale(std::wstring str) { sale = std::stoi(str); };
	void set_quantity(std::wstring str) { quantity = std::stoi(str); };
	void set_bookstoreId(std::wstring str) { bookstoreId = std::stoi(str); };
	void set_consumerId(std::wstring str) { consumerId = std::stoi(str); };
	void read(std::wifstream& file) { Read<ShoppingRecordDate>::Fun(*this, file); };

	std::wstring get_name() const { return name; };
	std::wstring get_time() const { return time; };
	std::wstring get_bookstoreName() const { return bookstoreName; };
	std::wstring get_sale() const { return std::to_wstring(sale); };
	std::wstring get_quantity()const { return std::to_wstring(quantity); };
	std::wstring get_bookstoreId() const{ return std::to_wstring(bookstoreId); };
	std::wstring get_consumerId() const { return std::to_wstring(consumerId); };
	void write(std::wofstream& file) const{ Write<ShoppingRecordDate>::Fun(*this, file); };
};


/*************************    ShoppingRecord           购物记录     ****************/
class ShoppingRecord {
	friend class Consumer;									//顾客友元

private:
	std::vector<ShoppingRecordDate> data;					//实际存购物记录数据的数组
	constexpr static char cs_postion[] = "ass";				//文件位置

private:
	ShoppingRecord() noexcept = default;
	inline void read();
	inline void write(std::vector<ShoppingRecordDate>& srArr);
	inline void find(std::vector<ShoppingRecordDate>& arr, int id);

//public:
//	ShoppingRecordDate& operator [](size_t k) { return data[k]; }			//[]
//	size_t size() { return data.size(); }									//size
//	bool empty() { return data.empty(); }									//empty
//	decltype(data.begin()) begin() { return data.begin(); }					//begin
//	decltype(data.end()) end() { return data.end(); }						//end
//	void push_back(ShoppingRecordDate& srd) { data.push_back(srd); };		//push_back
//	void emplace_back(ShoppingRecordDate& srd) { data.emplace_back(srd); };	//emplace_bakc

};

/********************        ShoppingRecord           购物记录    内联函数    ******************/
void ShoppingRecord::read() {
	ShoppingRecordDate srdata;
	std::wifstream file(cs_postion);
	while (!file.eof()) {
		srdata.read(file);
		if (srdata.name != L"")
			data.push_back(srdata);
	}
}

void ShoppingRecord::write(std::vector<ShoppingRecordDate>& srArr) {
	std::wofstream file(cs_postion, std::ios::app | std::ios::in);	//追加打开文件
	for (auto& srdata : srArr) {								//遍历所有新购物记入
		srdata.write(file);										//调用写函数
		data.push_back(srdata);									//同步到数据
	}
};

void ShoppingRecord::find(std::vector<ShoppingRecordDate>& arr, int id) {
	if (data.empty()) {read();}
	for (auto& it : data) {
		if (it.consumerId = id)
			data.push_back(it);
	}
}


/************************   Consumer          顾客 消费者      ************************/
class Consumer {
	friend class Read<Consumer>;							//读功能
	friend class Write<Consumer>;							//写功能
	friend class SingletonPattern<Consumer>;				//单例

public:
	Consumer() noexcept {};
	explicit Consumer(std::wstring str) : postion(str) { read(); }
	void reset(std::wstring str) { postion = str; read(); }
	void set(std::wstring str) { postion = str; read(); }
	bool pay(int k);

private:
	inline void read();										//
	inline void write() const;								//
	void set_name(std::wstring str) { name = str; };
	void set_portrait(std::wstring str) { portrait = str; };
	void set_id(std::wstring str) { id = std::stoi(str); };
	void set_balance(std::wstring str) { balance = std::stoi(str); };
	std::wstring get_name() const { return name; }
	std::wstring get_portrait() const { return portrait; }
	std::wstring get_id() const { return std::to_wstring(id); }
	std::wstring get_balance() const { return std::to_wstring(balance); }

public:
	std::vector<ShoppingRecordDate> shoppingRecord;			//消费记录
	std::wstring name;										//名字
	std::wstring portrait;									//头像
	int id = -1;											//id
	int balance = 0;										//余额

private:
	std::wstring postion;									//文件位置
};


/***********************       Consumer        顾客    内联函数        *********************/
void Consumer::read() {
	std::wifstream file(postion);
	Read<Consumer>::Fun(*this, file);

}

void Consumer::write() const {
	std::wofstream file(postion);
	Write<Consumer>::Fun(*this, file);

}

inline bool Consumer::pay(int k) {
	if (postion.empty())
		return false;
	int temp = balance - k;
	if (temp < 0)
		return false;
	balance = temp;
	write();
	return true;
}


/********************     ConsumerAdministrator       顾客管理       ********************/
class PageAdministrator;									//页面管理类 其他文件定义


class ConsumerAdministrator {
	friend class Read<ConsumerAdministrator>;				//读功能
	friend class Write<ConsumerAdministrator>;				//写功能
	friend class SingletonPattern<ConsumerAdministrator>;	//单例
	friend class PageAdministrator;							//页面管理类

public:
	struct PasswordData {
		friend class Read<PasswordData>;					//读功能
		friend class Write<PasswordData>;					//写功能
		std::wstring data;
		std::wstring name;

	private:
		void set_name(std::wstring str) { name = str; }
		void set_data(std::wstring str) { data = str; }
		std::wstring get_name() const { return name; }
		std::wstring get_data() const { return data; }

	public:
		void read(std::wifstream& file) { Read<PasswordData>::Fun(*this, file); }
		void write(std::wofstream& file) const { Write<PasswordData>::Fun(*this, file); }
	};

	struct ConsumerData {
		friend class Read<ConsumerData>;					//读功能
		friend class Write<ConsumerData>;					//写功能
		std::wstring name;
		std::wstring head;
		int id = -1;
		int balance = -1;

	private:
		void set_name(std::wstring str) { name = str; }
		void set_head(std::wstring str) { head = str; }
		void set_id(std::wstring str) { id = stoi(str); }
		void set_balance(std::wstring str) { balance = stoi(str); }
		std::wstring get_name() const { return name; }
		std::wstring get_head() const { return head; }
		std::wstring get_id() const { return std::to_wstring(id); }
		std::wstring get_balance() const { return std::to_wstring(balance); }

	public:
		void read(std::wifstream& file) { Read<ConsumerData>::Fun(*this, file); }
		void write(std::wofstream& file) const { Write<ConsumerData>::Fun(*this, file); }

	};

private:
	Consumer& consumer = *SingletonPattern<Consumer>::Get();	//用户引用
	std::unordered_map<std::wstring, std::wstring> passwordArr; //用户名 - 密码
	std::unordered_map<std::wstring, std::wstring> consumerArr; //用户名 - 消费者信息的文件
	std::wstring name;													//顾客名字
	std::wstring postion;												//顾客位置
	constexpr static char cs_password[] = "client\\password.txt";		//文件位置
	constexpr static char cs_consumer[] = "client\\consumer.txt";		//文件位置
	int idmax = 0;
	bool FlagInit = false;
	bool FlagEnter = false;


private:
	ConsumerAdministrator() {};
	~ConsumerAdministrator() {};
	void init();														//初始化
public:
	int enter(std::wstring& na, std::wstring& ke);						//登入
	int registerConsumer(std::wstring& key1, std::wstring& key2
							,std::wstring& name, std::wstring head);	//注册
};


/***************    ConsumerAdministrator       顾客管理    内联函数      ****************/
inline int ConsumerAdministrator::enter(std::wstring& na, std::wstring& ke) {
	if (passwordArr.find(na) == passwordArr.end()) {
		return 2;}									//2 用户名不存在
	if (passwordArr[na] == ke) {
		name = na;
		postion = consumerArr[na];
		consumer.reset(postion);
		FlagEnter = true;							//登入标志
		return 1;}									//1 成功
	else {
		return 3;}									//3 密码错误
}

inline void ConsumerAdministrator::init() {
	if (!FlagInit) {
		PasswordData cd;
		std::wifstream file(cs_consumer);
		while (!file.eof()) {
			cd.read(file);
			if (!cd.name.empty()) {
				consumerArr[cd.name] = cd.data;
				idmax++;
			}
		}

		PasswordData pd;
		std::wifstream file2(cs_password);
		while (!file2.eof()) {
			pd.read(file2);
			if (!pd.name.empty()) {
				passwordArr[pd.name] = pd.data;}
		}
		FlagInit = true;
	}
}

inline int ConsumerAdministrator::registerConsumer(std::wstring& key1, std::wstring& key2
								,std::wstring& name, std::wstring head) {
	if (name.empty())
		return 6;					//用户名为空
	if (key1 != key2)
		return 2;					//两次密码不相同
	if (passwordArr.find(name) != passwordArr.end())
		return 3;					//用户已经存在
	if (key1.size() < 1)
		return 4;					//密码为空
	for (auto& wstr : key1) {
		if (wstr == L' ')
			return 5;				//密码包含空格
	}
	//存用户名和密码
	PasswordData pd;				
	pd.name = name;
	pd.data = key1;
	std::wofstream file(cs_password,std::ios::app);
	pd.write(file);
	passwordArr[pd.name] = pd.data;
	//存用户名和文件位置
	pd.data = L"client\\consumers\\" + name;
	file.close();
	file.open(cs_consumer, std::ios::app);
	pd.write(file);
	consumerArr[pd.name] = pd.data;
	//存文件数据
	ConsumerData cd;
	cd.name = name;
	cd.id = ++idmax;
	cd.head = head;
	cd.balance = 0;
	file.close();
	file.open(pd.data, std::ios::app);
	cd.write(file);
	file.close();
	return 1;						//成功
}

